<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Plank\Mediable\Mediable;

class Post extends Model
{
    use Mediable;
    public function user(){
        return $this->belongsTo('App\User');
    }

}
