<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
       'status', 'firstName', 'email','phone','address','referred_by','total','subtotal','shipping','note', 'apartment','lastName','country_id','apartment','city','company'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'order_products','order_id','product_id')->withPivot('price','product_variation_id','quantity');
    }
//    public function marketer()
//    {
//        return $this->belongsTo(Admin::class,'referred_by');
//    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
