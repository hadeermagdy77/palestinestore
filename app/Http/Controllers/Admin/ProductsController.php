<?php
namespace App\Http\Controllers\Admin;
use App\CategoryProduct;
use App\Country;
use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\ProductAttribute;
use App\Models\ProductVariation;
use App\Models\Size;
use App\Partner;
use App\Product;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use MediaUploader;
use Plank\Mediable\Media;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::paginate(15);
       return view('admin.product.index', compact('products'));
    }
    public function status(Request $request , Product $product){
        $product->status =$request->status;
        $product->save();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesProduct=CategoryProduct::all();
        $attributes = Size::all();
        $brands = Partner::all();
        $countries = Country::all();

        return view('admin.product.create',compact('categoriesProduct','attributes','brands','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'price'=>'required',
            'category'=>'required',
            'discountPrice'=>'nullable',
            'start_date'=>'nullable|date|before:end_date',
            'end_date'=>'nullable|after:start_date',
            'img.*'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'variations.*.price' => 'numeric',
            'variations.*.quantity' => 'integer',
            'variations.*.name' =>'required_with:variations.*.price,variations.*.quantity',
        ]);

        $product = new Product();
        $product->name =  $request->input('name');
        $product->desc=$request->input('desc');
        //access request data
        $count =Product::max('id')+1;
        $product->slug =$request->input('name_en').' '.$count ;
        $product->product_cat_id =$request->input('category');
        $product->price =$request->input('price');
        $product->partner_id =$request->input('partner_id');
        $product->discountPrice =$request->input('discountPrice');
        $product->start_date =$request->input('start_date');
        $product->end_date =$request->input('end_date');
        $product->save();
        $product->countries()->attach($request->countries);

        //************************variations*******************
        $sizes  = $request->get('attributes');
        if (isset($sizes)) {
            $product->variation = true;
            foreach ($sizes as $size) {
                $product->sizes()->attach($size['id'], ['quantity' => $size['quantity']]);
            }
        }else{
            $product->variation = false;
            $product->quantity = $request->get('quantity');
        }

        //************************uploade photo*******************
        $files = $request->file('img');
        if ($request->hasFile('img')) {
            foreach ($files as $file){
                $media = MediaUploader::fromSource($file)->upload();
                $product->attachMedia($media, 'product');
            }
        }
        $product->save();
        return redirect()->route('admin.products.index')->with('success', "Item $product->name added successfully");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::find($id);
        $categoriesProduct=CategoryProduct::all();
        $brands=Partner::all();
        $attributes = $product->categoryProduct->attributes;
        $countries = Country::all();
        return view('admin.product.edit',compact('product','categoriesProduct','brands','attributes','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'price'=>'required',
            'category'=>'nullable',
            'discountPrice'=>'nullable',
            'start_date'=>'nullable|date|before:end_date',
            'end_date'=>'nullable|after:start_date',
            'img.*'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
        ]);



        $product =Product::find($id);

        $product->name =  $request->input('name');
        $product->desc=$request->input('desc');
        //access request data
        $product->product_cat_id =$request->input('category');
        $product->partner_id =$request->input('partner_id');
        $product->price =$request->input('price');
        $product->discountPrice =$request->input('discountPrice');
        $product->start_date =$request->input('start_date');
        $product->end_date =$request->input('end_date');
        $product->save();
        $product->countries()->sync($request->countries);
        $sizes = $request->get('attributes');
        if (isset($sizes)){
            $product->variation = true;
            $product->sizes()->detach();
            foreach ($sizes as $size){
                $product->sizes()->attach($size['id'],['quantity'=>$size['quantity']]);
            }
        } else{
            $product->variation = false;
            $product->quantity = $request->get('quantity');
        }

        //************************uploade photo*******************
        $files = $request->file('img');
        if ($request->hasFile('img')) {
            foreach ($files as $file){

                $media = MediaUploader::fromSource($file)->upload();
                $product->attachMedia($media, 'product');

            }
        }
        return redirect()->route('admin.products.index')->with('success', "Item $product->name updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        $product->delete();
        return redirect()->back()->with('success','Product deleted successfully');
    }
    public function deleteImage(Media $media)
    {
        $media->delete();
        return redirect()->back()->with('success', "image removed successfully");
    }
}
