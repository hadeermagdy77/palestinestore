<?php

namespace App\Http\Controllers\Admin;
use App\CategoryProduct;
use App\Country;
use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use RealRashid\SweetAlert\Facades\Alert;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function index()
    {
        $users = Admin::all()->except(1);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::where('guard_name', 'admin')->get();

        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'password' => 'required|max:25|min:6|confirmed',
            'email' => 'required|email|max:255|unique:admins',
            'role' => 'required',
        ]);
//dd($request);
//        $password = str_random();

        $user = new Admin();

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->syncRoles($request->get('role'));

        $user->save();
        Alert::success('User '.$user->name .' Created successfully');
        return redirect('admin/users/all');
    }

    public function edit(Admin $user)
    {
        $roles = Role::where('guard_name', 'admin')->get();
        return view('admin.users.edit', compact('roles', 'user'));
    }

    public function update(Request $request, Admin $user)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'roles' => 'required',
            'password' => 'nullable|max:25|min:6|confirmed',
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if($request->password){
            $user->password =Hash::make( $request->get('password'));
        }
        $user->syncRoles($request->get('roles'));

        $user->save();

        return redirect()->route('admin.users.all')->with('success', "User $user->name updated successfully");
    }

    public function destroy(Admin $user)
    {
        if($user->account_id){
            Alert::error(' Can`t delete User '.$user->name .' ');
        }else{
            $user->delete();
            Alert::success('User '.$user->name .' deleted successfully');
        }
        return redirect('admin/users/all');
    }
}
