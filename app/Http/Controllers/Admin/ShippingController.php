<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Country;
use App\Shiping;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        $shipping=Country::all();
//        return view('admin.shiping.shiping',compact('shipping'));
//    }
//
//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//        return view('admin.shiping.create');
//    }
//

//
//    /**
//     * Display the specified resource.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
//    public function show(ShippingCompany $company)
//    {
//        $orders = $company->orders;
//        return view('admin.shippingCompanies.show',compact('company','orders'));
//    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'shipPrice'=>'nullable',

        ]);
        $shiping=new Shiping();
        $shiping->name=$request->input('name');
        $shiping->shipPrice=$request->input('shipPrice');
        $shiping->save();
        return  redirect()->back()->with('success'," $shiping->name added successfully");

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $cities=Country::all();
        return view('admin.shiping.edit',compact('cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {

        $request->validate([
            'name'=>'required',
            'shipPrice'=>'nullable',
        ]);
        $country->name=$request->input('name');
        $country->price=$request->input('shipPrice');
        $country->save();
        return  redirect()->back()->with('success'," $country->name updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
//        $shiping= Country::find($id);
        $country->delete();
        return redirect()->back();

    }
}
