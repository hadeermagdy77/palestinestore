<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Instagram;
use MediaUploader;

class InstagramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instagrams =Instagram::all();
        return view('admin.instagram.index', compact('instagrams'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.instagram.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'img'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'link'=>'url',

        ]);
        $instagram = new Instagram();
        $instagram->link =$request->input('link');

        $instagram->save();

        //************************uploade photo*******************
        if($request->file('img'))
        {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $instagram->attachMedia($media, 'instagram');
        }
        return redirect('admin/instagrams')->with('success', "instagram  added successfully");

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instagram=Instagram::find($id);
        return view('admin.instagram.edit',compact('instagram'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'img'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'link'=>'url',

        ]);
        $instagram=Instagram::find($id);

        $instagram->link =$request->input('link');

        $instagram->save();

        //************************uploade photo*******************
        if($request->file('img'))
        {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $instagram->attachMedia($media, 'instagram');
        }

        return redirect('admin/instagrams')->with('success', "instagram  added successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instagram=Instagram::find($id);
        $instagram->delete();

        return  redirect('admin/instagrams')->with('deleted','instagram deleted successfully');
    }
}
