<?php

namespace App\Http\Controllers\Admin;
use App\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MediaUploader;

class SectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active=Section::where('active', 1)->orderBy('order')->get();
        $deActive=Section::where('active', 0)->get();

        return view('admin.sortSection.index',compact('active','deActive'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        if ($request->active) {
            foreach ($request->active as $section) {
                $getSection = Section::find($section['id']);
                $getSection->order = $section['order'];
                $getSection->active = 1;
                $getSection->save();

            }
        }
        if ($request->deActive) {
            foreach ($request->deActive as $section) {
                $getSection = Section::find($section['id']);
                $getSection->active = 0;
                $getSection->save();

            }
        }
        return redirect()->back();
//        dd(SortSection::all());
    }


// update section setting
    public function update(Request $request){
        //validation


        $request->validate([
            'name' => 'nullable',
            'description'=> 'nullable',
            'img'=>'image|mimes:jpg,png,jpeg|max:10048|nullable',
            'backgroundImage'=>'image|mimes:jpg,png,jpeg|nullable',
            'type'=>'required',
        ]);

        $aboutUs = Section::Where('type',$request->type)->first();


            $aboutUs->name=$request->input('name');
            $aboutUs->description=$request->input('description');

        $aboutUs->save();
        //************************uploade image background*******************

        if($request->file('backgroundImage')) {
            $media = MediaUploader::fromSource($request->file('backgroundImage'))->upload();
            $aboutUs->syncMedia($media, 'backgroundImage');
        }
        //************************uploade photo*******************
        if($request->file('img')) {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $aboutUs->syncMedia($media, 'about');
        }
        return redirect()->back()->with('success', "setting section added successfully");

    }

}
