<?php

namespace App\Http\Controllers\Admin;;
use App\Section;
use App\CategoryProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MediaUploader;

class CategoryProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $categoriesProduct=CategoryProduct::all();
        $categorySection= Section::Where('type','catProduct')->first();
        return view('admin.product.categoriesProduct',compact('categoriesProduct','categorySection'));
    }
   public function setting(){
       $categorySection= Section::Where('type','catProduct')->first();
       return view('admin.product.setting',compact('categorySection'));

   }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesProduct=CategoryProduct::all();
        return view('admin.product.createCategoryProduct',compact('categoriesProduct'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'nullable',
            'img'=>'required|image|mimes:jpg,png,jpeg,svg|max:3000',
        ]);

        $categoryProduct = new CategoryProduct();

            $categoryProduct-> name = $request->input('name');
            $categoryProduct->desc =  $request->input('desc');

        //access request data

        $categoryProduct->save();

        //************************uploade photo*******************
        if($request->file('img'))
        {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $categoryProduct->attachMedia($media, 'catproduct');
        }
        return redirect()->back()->with('success','Category product created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function attributes(CategoryProduct $category )
    {
        $sub  = $category->attributes()->pluck('name','id');
        return  json_encode($sub);
    }
    /**
     * Show the form for editing the specified resourc

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'name' => 'required',
            'desc' => 'nullable',
            'img'=>'required|image|mimes:jpg,png,jpeg,svg|max:3000',
        ]);
        $categoryProduct =CategoryProduct::find($id);

        $categoryProduct-> name = $request->input('name');
        $categoryProduct->desc =  $request->input('desc');

        $categoryProduct->save();

        //************************uploade photo*******************
        if($request->file('img'))
        {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $categoryProduct->attachMedia($media, 'catproduct');
        }
        return redirect()->back()->with('success','Category product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryProduct=CategoryProduct::find($id);


            $categoryProduct->delete();
            return  redirect()->back()->with('success','Category product deleted successfully');


    }
}
