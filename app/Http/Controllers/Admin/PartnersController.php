<?php

namespace App\Http\Controllers\Admin;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Plank\Mediable\Media;
use MediaUploader;


class PartnersController extends Controller
{
    public function partners(){
        return view('admin.home.partners.index');
    }

    public function storeLogo(Request $request){
        //validation
        $request->validate([
            'name'=>'required',
            'desc'=>'nullable',
            'img'=>'image|mimes:jpg,png,jpeg|max:3000',
        ]);
        $partners=new Partner();
        $partners->name=$request->input('name');
        $partners->desc=$request->input('desc');
        $partners->save();
        //************************uploade photo*******************
        if($request->file('img')) {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $partners->syncMedia($media, 'partnersLogo');
        }

        return redirect()->back()->with('success', "brand logo added successfully");
    }

    public function showImages(){

        $partnersLogo =Partner::all();
        return view('admin.home.partners.images',compact('partnersLogo'));
    }


    public function update(Request $request, Partner $image){
        //validation

        $request->validate([
            'name'=>'required',
            'desc'=>'nullable',
            'img'=>'image|mimes:jpg,png,jpeg|max:2048',
        ]);
        $image->name=$request->input('name');
        $image->desc=$request->input('desc');
        $image->save();

        //************************uploade photo*******************
        if($request->file('img')) {
            $media = MediaUploader::fromSource($request->file('img'))->upload();
            $image->syncMedia($media, 'partnersLogo');
        }

        return redirect()->back()->with('success', "brand Logo updated successfully");
    }
    public function delete(Partner $image){

        $image->delete();
        return redirect()->back()->with('success','img deleted successfully');
    }

    public function deleteBackground(Partner $image){
        $image->delete();
        return redirect()->back()->with('success','Bachground image deleted successfully');
    }
}
