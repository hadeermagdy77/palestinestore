<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
      public function index()
    {
        $messages=Message::all();
        foreach (Message::where('views',0)->get() as $message){
            $message->views =1;
            $message->save();
        }
        return view('admin.messages',compact('messages'));
    }
}
