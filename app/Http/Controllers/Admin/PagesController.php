<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Requests;
use Illuminate\Support\Str;
use App\Page;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('admin.pages.index', compact('pages'));
    }
    public function pageRequest(){
        $requests=Requests::whereNotNull('page_id')->orderBy('id', 'DESC')->get();
        foreach ($requests->where('views',0) as $request){
            $request->views =1;
            $request->save();
        }
        return view('admin.pages.requests',compact('requests'));
    }
    public function create()
    {
        $pages=Page::all();
        return view('admin.pages.create',compact('pages'));
    }

    public function store(Request $request)
    {
            $request->validate([
                'name' => 'required',
                'content' => 'nullable'

            ]);
//dd($request->all());
        $page = new Page();

            $page->name= $request->input('name');
            $page->content =  $request->input('content');
        $page->slug = $request->input('hasLink') == 'on' ?null: Str::slug($request->input('name'))  ;
        $page->inNav = $request->input('inNav') == 'on' ? true : false;
        $page->inFooter = $request->input('inFooter') == 'on' ? true : false;
        $page->published = $request->input('published') == 'on' ? true : false;
        $page->published_at = $request->input('published_at');

        $page->save();
        return redirect()->route('admin.pages.index')->with('success', "Page $page->name created successfully");
    }

    public function edit(Page $page)
    {
        $pages=Page::all()->except($page->id);

        return view('admin.pages.edit', compact('page','pages'));
    }

    public function update(Request $request, Page $page)
    {
            $request->validate([
                'name' => 'required',
                'content' => 'nullable',

            ]);


        $page->name= $request->input('name');
        $page->content =  $request->input('content');

        $page->slug = $request->input('hasLink') == 'on' ?null: Str::slug($request->input('name'))  ;
        $page->inFooter = $request->input('inFooter') == 'on' ? true : false;
        $page->inNav = $request->input('inNav') == 'on' ? true : false;
        $page->published = $request->input('published') == 'on' ? true : false;
        $page->published_at = $request->input('published_at');

        $page->save();

        return redirect()->route('admin.pages.index')->with('success', "Page $page->name updated successfully");
    }

    public function destroy(Page $page)
    {
            $page->delete();
            return redirect()->route('admin.pages.index')->with('success', "Page $page->name deleted successfully");

    }
}
