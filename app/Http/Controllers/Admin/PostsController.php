<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Requests;
use App\Section;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MediaUploader;
use Plank\Mediable\Media;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts =Post::all();
        $blogs= Section::Where('type','blog')->first();
        return view('admin.posts.index', compact('posts','blogs'));
    }
    public function setting(){
        $blog= Section::Where('type','blog')->first();
        return view('admin.posts.setting',compact('blog'));

    }
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //validation
        $request->validate([
            'img.*'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'subject' => 'required',
            'desc' => 'required',
        ]);

        $post = new Post();

        //access request data
        $post->subject =$request->input('subject');
        $post->desc=$request->input('desc');
        $post->save();

        //************************uploade photo*******************
        $files = $request->file('img');
        if ($request->hasFile('img')) {
            foreach ($files as $file){

                $media = MediaUploader::fromSource($file)->upload();
                $post->attachMedia($media, 'post');

            }
        }

            return redirect('admin/posts')->with('success','blog created successfully');


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::find($id);
        return view('admin.posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::find($id);
        return view('admin.posts.edit',compact('post'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $request->validate([
            'img.*'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'subject' => 'required',
            'desc' => 'required',
        ]);

        $post=Post::find($id);
        //access request data
        $post->subject =$request->input('subject');
        $post->desc=$request->input('desc');
        $post->save();

        //************************uploade photo*******************
        $files = $request->file('img');
        if ($request->hasFile('img')) {
            foreach ($files as $file){

                $media = MediaUploader::fromSource($file)->upload();
                $post->attachMedia($media, 'post');

            }
        }

        return redirect('admin/posts')->with('success','blog created successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {

        $post=Post::find($id);
         $post->delete();

            return  redirect('admin/posts')->with('deleted','blog deleted successfully');

    }
    public function deleteImage(Media $media)
    {
        $media->delete();
        return redirect()->back()->with('success', "image removed successfully");
    }
}
