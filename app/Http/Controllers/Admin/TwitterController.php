<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Twitter;
use MediaUploader;
use Plank\Mediable\Media;

class TwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $twitters=Twitter::all();
        return view('admin.twitter.index', compact('twitters'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.twitter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'img.*'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'twitterLink'=>'url',

        ]);
        $twitter = new Twitter();
        $twitter->name =$request->input('name');
        $twitter->twitterLink =$request->input('twitterLink');

        $twitter->save();

        //************************uploade photo*******************

        $files = $request->file('img');
        if ($request->hasFile('img')) {
            foreach ($files as $file){

                $media = MediaUploader::fromSource($file)->upload();
                $twitter->attachMedia($media, 'twitter');

            }
        }
        return redirect('admin/twitters')->with('success', "twitter  added successfully");

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $twitter=Twitter::find($id);
        return view('admin.twitter.edit',compact('twitter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'img.*'=>'image|mimes:jpg,png,jpeg,svg|max:3000',
            'twitterLink'=>'url',

        ]);
        $twitter=Twitter::find($id);

        $twitter->name =$request->input('name');
        $twitter->twitterLink =$request->input('twitterLink');

        $twitter->save();

        //************************uploade photo*******************
        $files = $request->file('img');
        if ($request->hasFile('img')) {
            foreach ($files as $file){

                $media = MediaUploader::fromSource($file)->upload();
                $twitter->attachMedia($media, 'twitter');

            }
        }
        return redirect('admin/twitters')->with('success', "twitter  added successfully");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $twitter=Twitter::find($id);
        $twitter->delete();

        return  redirect('admin/twitters')->with('deleted',"twitter $twitter->name deleted successfully");
    }
    public function deleteImage(Media $media)
    {
        $media->delete();
        return redirect()->back()->with('success', "image removed successfully");
    }
}
