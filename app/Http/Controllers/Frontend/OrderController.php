<?php

namespace App\Http\Controllers\Frontend;

use App\Country;
use App\Http\Controllers\Controller;

use App\Models\ProductVariation;
use App\Order;
use App\Product;
use App\UserAddress;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Srmklive\PayPal\Services\ExpressCheckout;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        $countries = Country::all();
        return view('frontend.checkout',compact('countries'));
    }
    public function addToCart(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'qty'=>'required|integer|min:1',
        ]);
        $product = Product::find($request->product);

        if ($product->sizes->count() > 0){
            $type = $product->sizes->where('id', $request->size)->first();
//         dd($type->pivot->quantity);
            $content = Cart::content()->where('id',$product->id);
            if($content){
                foreach ($content as $item){
                    if($item->options->first() == $type->id && $type->pivot->quantity < $item->qty+$request->qty){
                        return redirect()->back()->with('error', __('This Qty Not In Stock'));
                    }
                }
            }
            if($type && $type->pivot->quantity >= $request->qty){
                Cart::add($request->product, $product->name, $request->qty , $product->discount? $product->discount:$product->price,0,['size' => $type->id]);
//            dd(Cart::content());
                return redirect()->back()->with('success', __('Item Added'));
            } else {
                return redirect()->back()->with('error', __('Item Not In Stock'));
            }
        }else{
//            dd($product->quantity);
            if( $product->quantity >= $request->qty){
                Cart::add($request->product, $product->name, $request->qty, $product->discount? $product->discount:$product->price ,0);
                return redirect()->back()->with('success', __('Item Added'));
            } else {
                return redirect()->back()->with('error', __('Item Not In Stock'));
            }
        }

    }


    public function deleteFromCart($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back()->with('success','Done');
    }

    public function updateAddress($address_id)
    {
        $user =  auth()->user();
        if ($address_id == 0){

            $user->active_address_id = null;
        }else{
            $user->active_address_id = $address_id;
        }
        $user->save();
//        return true;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'firstName' => 'required',
            'email'=>'email|required',
            'phone'=>'required',
            'address'=>'required',
            'city'=>'required',
            'country'=>'required',
            'note'=>'nullable',
            'zip'=>'integer',
        ]);
//        dd($request->all());
        $subTotal =  (float)str_replace(',','',Cart::subtotal());

        if (Cart::count() > 0 ){
            $shipping = Country::find($request->country)->price ? Country::find($request->country)->price : setting('general.shipping') ;
            $data = [];
            $data['items'] = [];
            $user = auth()->user();
            $user->update([
                'firstName'=>$request->firstName,
                'lastName'=>$request->lastName,
                'phone'=>$request->phone,
                'zip'=>$request->zip,
                'country_id'=>$request->country,
                'company'=>$request->company,
                'address'=>$request->address,
                'city'=>$request->city,
                'apartment'=>$request->apartment
            ]);
            if($request->address_id == 0) {
                UserAddress::Create(

                    [
                        'firstname' => $request->firstName,
                        'lastName' => $request->lastName,
                        'phone' => $request->phone,
                        'address1' => $request->address1,
                        'address2' => $request->address2,
                        'company' => $request->company,
                        'city' => $request->city,
                        'apartment' => $request->apartment,
                        'zip' => $request->zip,
                        'country' => $request->country,
                        'user_id' => $user->id
                    ]

                );
            }


//            $request->merge(['total'=>$subTotal]);
//            $order =   Order::Create($request->all());
            foreach (Cart::content() as $item) {
                $product = Product::find($item->id);
                if($product->countries->where('id',$request->country)->count() == 0){
                    return redirect()->back()->with('error','Item '.$product->name.' Do not ship to your country');
                }
                if ($product->sizes->count() > 0 ) {
                    $type = $item->options->first();
                    $size = $product->sizes->where('id', $type)->first();
                    if ($size && $size->pivot->quantity >= $item->qty) {
                        $itemData = [
                            'name' => $item->name,
                            'qty' => $item->qty,
                            'price' => $item->price ,
                        ];
                        $data['items'] [] = $itemData;
                    } else {
                        return redirect()->back()->with('error', 'sorry the quantity of ' . $product->name . ' ' . $size->name . ' product not allowed now please try again leter');
                    }
                }else{
                    if ($product->quantity >= $item->qty ){
                        $itemData = [
                            'name' => $item->name,
                            'qty' => $item->qty,
                            'price' => $item->price ,
                        ];
                        $data['items'] [] = $itemData;
                    } else {
                        return redirect()->back()->with('error', 'sorry the quantity of ' . $product->name .' product not allowed now please try again leter');
                    }
                }
            }
        }else{
            return redirect()->back()->with('error','Not Items In Your Cart');
        }
        $data['invoice_id'] = Order::max('id')+500;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('payment.success');
        $data['cancel_url'] = route('payment.cancel');
        $data['subtotal'] = $subTotal ;
        $data['shipping'] = $shipping ;
        $data['total'] = $subTotal + $shipping;
//        dd($data);
//        dd('dd');
        $provider = new ExpressCheckout;
        $response = $provider->setExpressCheckout($data,true);
//        dd($response);
        return redirect($response['paypal_link']);
    }

    public function update(Request $request,$rowId) {
        $item = Cart::Content()->where('rowId',$rowId)->first();
//        dd($item);
        $product = Product::find($item->id);
        if ($product->sizes->count() > 0 ) {
            $type = $item->options->first();
            $size = $product->sizes->where('id', $type)->first();
            if ($size && $size->pivot->quantity >= $request->qty) {
                Cart::update($rowId, $request->qty);
            } else {
                return redirect()->back()->with('error', 'sorry the quantity of ' . $product->name . ' ' . $size->name . ' product not allowed now please try again leter');
            }
        }else{
            if ($product->quantity >= $request->qty ){
                Cart::update($rowId, $request->qty);
            } else {
                return redirect()->back()->with('error', 'sorry the quantity of ' . $product->name .' product not allowed now please try again leter');
            }
        }
        return redirect()->back()->with('success','updated done');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Frontend\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function orderNow(Request $request)
    {
//        dd($request);
        $referred_by = Cookie::get('referral');
//        dd($referred_by);
        //validation
        $request->validate([
            'name' => 'required',
            'email'=>'email|required',
            'phone'=>'required',
            'address'=>'required',
            'note'=>'nullable',
            'product_id'=>'required'
        ]);
//        $subTotal =  (float)str_replace(',','',Cart::subtotal());

        $product = Product::find($request->product_id);
        $request->merge(['referred_by'=>$referred_by,'total'=>$product->discount?$product->discount:$product->price]);
        $order =   Order::Create($request->all());
        $order->products()->attach($request->product_id, ['price' => $product->discount?$product->discount:$product->price, 'provider_id' => $product->provider_id]);
        return redirect('/')->with('success','Order Created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Frontend\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Frontend\Order  $order
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, Order $order)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Frontend\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
