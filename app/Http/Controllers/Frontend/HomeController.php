<?php

namespace App\Http\Controllers\Frontend;

use App\CategoryProduct;
use App\Country;
use App\Http\Controllers\Controller;
use App\Instagram;
use App\Models\Size;
use App\Partner;
use App\Post;
use App\Product;
use App\Section;
use App\Slider;

use App\productsCategory;
use App\Twitter;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use phpDocumentor\Reflection\Location;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $slider  = Slider::all();
        $partnersLogo=Partner::all();
        $posts  =Post::all();
        $categoryText = Section::Where('type','blog')->first();
        $activeSection=Section::Where('active',1)->orderBy('order')->get();
        $catProduct= Section::Where('type','catProduct')->first();
        $products=Product::where('status',1)->with('categoryProduct')->get()->take(12);
        $categoriesProduct=CategoryProduct::orderBy('id', 'DESC')->get();
        $brands=Partner::all();
        $instagrams=Instagram::all();
        $twitters=Twitter::all();
        return view('frontend.index',compact('slider','posts','partnersLogo','categoryText','activeSection','products','categoriesProduct','catProduct','brands','instagrams','twitters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function brands(){
        $brands=Partner::all();
        $attributes = Size::all();
        return view('frontend.Brands',compact('brands','attributes'));
    }

    public function brand(Partner $brand,Request $request){

        $products=$brand->products;
        $attributes = Size::all();
        if ($request->method() == 'GET') {
            if(!($request->filter)){
                $products=$brand->products()->where('status',1)->paginate(30);
            }elseif($request->filter == 'lowPrice'){
                $products= $brand->products()->where('status',1)->OrderBy('price','asc')->paginate(30);
            }elseif($request->filter == 'highPrice'){
                $products= $brand->products()->where('status',1)->OrderBy('price','desc')->paginate(30);
            }else{
                $products=$brand->products()->where('status',1)->OrderBy('name','asc')->paginate(30);
            }
        }else {
            $price = (explode(' - ',str_replace('$','',$request->price)));
            $products = $brand->products()->where('status', 1)->whereBetween('price', [$price[0], $price[1]])->paginate(30);
        }
        return view('frontend.collections',compact('brand','products','attributes'));

    }
    public function sizeBrand(Partner $brand,Size $size,Request $request){
        $products=$size->products->where('partner_id',$brand->id);
        $attributes=Size::all();
        return view('frontend.collections',compact('attributes','products','brand','size'));
    }

    public function sale(Request $request){

//        $products=Product::where('status',1)->where('discountPrice','>' ,0 );
//        $products  = $products->where('discountPrice','>' ,0 );
        $attributes = Size::all();

        if ($request->method() == 'GET') {
            if(!($request->filter)){
                $products=Product::where('status',1)->where('discountPrice','>' ,0 )->get();
            }elseif($request->filter == 'lowPrice'){
                $products=Product::where('status',1)->where('discountPrice','>' ,0 )->OrderBy('price','asc')->get();
            }elseif($request->filter == 'highPrice'){
                $products=Product::where('status',1)->where('discountPrice','>' ,0 )->OrderBy('price','desc')->get();
            }else{
                $products=Product::where('status',1)->where('discountPrice','>' ,0 )->OrderBy('name','asc')->get();
            }
        }else {
            $price = (explode(' - ',str_replace('$','',$request->price)));
             $products=Product::where('status',1)->where('discountPrice','>' ,0 )->whereBetween('price', [$price[0], $price[1]])->paginate(30);
        }
        return view('frontend.sale',compact('products','attributes'));
    }

    /** view blog  */
    public function blogg(){
        $posts =Post::all();
        $categoryText = Section::Where('type','blog')->first();
        return view('frontend.blogg',compact('posts','categoryText'));
    }

    public function blog(Post $post)
    {
        $posts =Post::all();
        return view('frontend.blog',compact('post','posts'));
    }
    public function newReleases(Request $request){
        $products = Product::where('status', 1)->orderBy('id', 'desc')->paginate(30);
        $attributes = Size::all();
        if ($request->method() == 'GET') {
            if(!($request->filter)){
                $products=Product::where('status',1)->paginate(30);
            }elseif($request->filter == 'lowPrice'){
                $products= Product::where('status',1)->OrderBy('price','asc')->paginate(30);
            }elseif($request->filter == 'highPrice'){
                $products= Product::where('status',1)->OrderBy('price','desc')->paginate(30);
            }else{
                $products=Product::where('status',1)->OrderBy('name','asc')->paginate(30);
            }
        }else {
            $price = (explode(' - ',str_replace('$','',$request->price)));
            $products = Product::where('status', 1)->whereBetween('price', [$price[0], $price[1]])->paginate(30);
        }
//        dd($products);
        return view('frontend.newReleases',compact('products','attributes'));
    }
    public function categoriesProduct(Request $request){
        $categoriesProduct=CategoryProduct::all();
        $attributes = Size::all();
        $catProduct=Section::Where('type','catProduct')->first();
        $products = Product::where('status', 1)->paginate(30);
        if ($request->method() == 'GET') {
            if(!($request->filter)){
                $products=Product::where('status',1)->paginate(30);
            }elseif($request->filter == 'lowPrice'){
                $products= Product::where('status',1)->OrderBy('price','asc')->paginate(30);
            }elseif($request->filter == 'highPrice'){
                $products= Product::where('status',1)->OrderBy('price','desc')->paginate(30);
            }else{
                $products=Product::where('status',1)->OrderBy('name','asc')->paginate(30);
            }
        }else {
            $price = (explode(' - ',str_replace('$','',$request->price)));
            $products = Product::where('status', 1)->whereBetween('price', [$price[0], $price[1]])->paginate(30);
        }
        return view('frontend.catproduct',compact('categoriesProduct','attributes','catProduct','products'));
    }
    public function productsCategory(CategoryProduct $category,Request $request){
        $categoriesProduct=CategoryProduct::all();
        $attributes = $category->attributes()->whereHas('products')->get();
        $catProduct=Section::Where('type','catProduct')->first();
        if ($request->method() == 'GET') {
            if(!($request->filter)){
                $products= $category->products()->where('status',1)->paginate(30);
            }elseif($request->filter == 'lowPrice'){
                $products= $category->products()->where('status',1)->OrderBy('price','asc')->paginate(30);
            }elseif($request->filter == 'highPrice'){
                $products= $category->products()->where('status',1)->OrderBy('price','desc')->paginate(30);
            }else{
                $products= $category->products()->where('status',1)->OrderBy('name','asc')->paginate(30);
            }
        }else {
            $price = (explode(' - ',str_replace('$','',$request->price)));
            $products = $category->products()->where('status', 1)->whereBetween('price', [$price[0], $price[1]])->paginate(30);
        }
        return view('frontend.category',compact('categoriesProduct','attributes','category','products','catProduct'));

    }

    public function size(Size $size,Request $request){
        $products=$size->products;
        $sizes=Size::all()->except($size->id);
        if ($request->method() == 'GET') {
            if(!($request->filter)){
                $products=$size->products()->where('status',1)->paginate(30);
            }elseif($request->filter == 'lowPrice'){
                $products= $size->products()->where('status',1)->OrderBy('price','asc')->paginate(30);
            }elseif($request->filter == 'highPrice'){
                $products= $size->products()->where('status',1)->OrderBy('price','desc')->paginate(30);
            }else{
                $products=$size->products()->where('status',1)->OrderBy('name','asc')->paginate(30);
            }
        }else {
            $price = (explode(' - ',str_replace('$','',$request->price)));
            $products =$size->products()->where('status', 1)->whereBetween('price', [$price[0], $price[1]])->paginate(30);
        }
        return view('frontend.sizes',compact('size','sizes','products'));

    }


    public function product($slug){

        $product = Product::where('slug',$slug)->firstOrFail();
        $cat= $product->categoryProduct;
        $products= $cat->products->except($product->id)->take(4);
        return view('frontend.product',compact('product','cat','products'));
    }

   public function quickview(Product $product){
        return view('frontend.quickview',compact('product'));
   }


  public function  account(){

    return view('frontend.myAcount');

  }

    public function search(Request $request )
    {

        $categoriesProduct=CategoryProduct::all();
        $catProduct=Section::Where('type','catProduct')->first();
        $attributes = Size::all();
        $name = $request->search;

        $products = Product::where('name', 1)->paginate(20);

        $products = Product::where(['status' => 1])->where('name', 'like', '%' . $name . '%')->paginate(20);

        return view('frontend.catproduct',compact('categoriesProduct','catProduct','products','attributes'));
    }


    public function cart(){
        return view('frontend.cart');
    }

//    public function address(){
//        return view('frontend.address');
//    }
    public function sortSection()
    {
        $activeSection=Section::Where('active',1)->orderBy('order')->get();
    }
}
