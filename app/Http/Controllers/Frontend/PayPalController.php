<?php

namespace App\Http\Controllers\Frontend;

use App\Admin;
use App\Country;
//use App\Mail\SendOrderToSeller;
//use App\Mail\SendOrderToUser;
//use App\Models\ProductVariation;
use App\Order;
use App\Product;
//use Gloudemans\Shoppingcart\Cart;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Srmklive\PayPal\Services\ExpressCheckout;

class PayPalController extends Controller
{

    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        return redirect('/checkout')->with('error','Your Payment Is Canceled.');
    }

    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        if (Cart::count() > 0) {

            $subTotal = (float)str_replace(',', '', Cart::subtotal());
            $shipping = auth()->user()->country->price ? auth()->user()->country->price : setting('general.shipping');
//            $country = Country::find(Cart::content()->first()->options['country']);
            $data = [];
            $data['items'] = [];
            $order = Order::Create([
                'user_id' => auth()->user()->id,
                'firstName'=>auth()->user()->name,
                'lastName'=>auth()->user()->lastName,
                'email'=>auth()->user()->email,
                'address'=>auth()->user()->address,
                'phone'=>auth()->user()->phone,
                'city'=>auth()->user()->city,
                'country_id'=>auth()->user()->country_id,
                'company'=>auth()->user()->company,
                'apartment'=>auth()->user()->apartment,
                'zip'=>auth()->user()->zip,
                'subtotal' => $subTotal,
                'shipping' => $shipping,
                'total' => $subTotal + $shipping
            ]);
            foreach (Cart::content() as $item) {
                $product = Product::find($item->id);

                if ($product->sizes->count() >0 ) {
                    $type = $item->options->first();
                    $size = $product->sizes->where('id', $type)->first();
//                    dd($size );
                    if ($size && $size->pivot->quantity >= $item->qty) {
                        $order->products()->attach($item->id, ['price' => $item->price, 'quantity' => $item->qty, 'product_variation_id' => $type]);
//                        dd($size->pivot->quantity);
                        $size->pivot->decrement('quantity', $item->qty);
                        $itemData = [
                            'name' => $item->name,
                            'qty' => $item->qty,
                            'price' => $item->price ,
                        ];
                        //                    dd($itemData);
                        $data['items'] [] = $itemData;
                        Cart::remove($item->rowId);
                    } else {
                        $order->delete();
                        return redirect()->back()->with('error', 'sorry the quantity of ' . $product->name . ' ' . $size->name . ' product not allowed now please try again leter');
                    }
                }else{

                    if ($product->quantity >= $item->qty ){
                        $order->products()->attach($item->id, ['price' => $item->price, 'quantity' => $item->qty]);
                        $product->decrement('quantity', $item->qty);
                        $itemData = [
                            'name' => $item->name,
                            'qty' => $item->qty,
                            'price' => $item->price ,
                        ];
                        //                    dd($itemData);
                        $data['items'] [] = $itemData;
                        Cart::remove($item->rowId);
                    } else {
                        $order->delete();
                        return redirect()->back()->with('error', 'sorry the quantity of ' . $product->name .' product not allowed now please try again leter');
                    }

                }
            }


        } else {
            return redirect()->back()->with('error', 'Not Items In Your Cart');
        }


//dd($data);

        $data['invoice_id'] = $order->id;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['subtotal'] = $subTotal ;
        $data['shipping'] = $shipping;
        $data['total'] = $subTotal + $shipping;
        $provider = new ExpressCheckout;
        $PayerID = $request->PayerID;
        $response = $provider->getExpressCheckoutDetails($request->token);
        $response = $provider->doExpressCheckoutPayment($data, $request->token, $PayerID);

        return redirect('/')->with('success','Order Created successfully');

    }
}
