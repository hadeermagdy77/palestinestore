<?php

namespace App\Http\Controllers\Frontend;

use App\Country;
use App\User;
use App\UserAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view('frontend.address',compact('countries'));

    }
    public function profile(){
        $user=Auth::user();
        $addresses=$user->addresses;

        return view('frontend.profile',compact('addresses','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'firstname' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'company' => 'required',
            'city' => 'required',
            'apartment' => 'nullable',
            'zip' => 'integer',
            'country' => 'nullable'

        ]);
        $user = auth()->user();
        $request->merge(['user_id'=>$user->id]);
        $request->offsetUnset('_token');
//        $request->merge(['country'=>$user->id]);
        UserAddress::Create($request->all());

        return redirect()->route('profile')->with('success', " $user->name added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address=UserAddress::find($id);
        $countries = Country::all();
        return view('frontend.editaddress',compact('address','countries'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'phone' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'company' => 'required',
            'city' => 'required',
            'apartment' => 'nullable',
            'zip' => 'integer',
            'country' => 'nullable'

        ]);
        $address=UserAddress::find($id);

        $address->phone =  $request->input('phone');
        $address->address1 =  $request->input('address1');
        $address->address2 =  $request->input('address2');
        $address->company =  $request->input('company');
        $address->city =  $request->input('city');
        $address->apartment =  $request->input('apartment');
        $address->country =  $request->input('country');
        $address->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $addresses=UserAddress::find($id);
        $addresses->delete();
        return redirect()->back()->with('success','Address deleted successfully');
    }
}
