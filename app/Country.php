<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable=['name','code','code2','continent','price'];
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_countries','country_id','product_id');
    }
}
