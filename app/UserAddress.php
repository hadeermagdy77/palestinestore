<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'firstname', 'lastName','phone','address1', 'address2','company', 'city','apartment','zip','country','user_id'

    ];
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
