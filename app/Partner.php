<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Partner extends Model
{
    //
    use Mediable;
    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
