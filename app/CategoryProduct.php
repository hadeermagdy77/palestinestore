<?php

namespace App;

use App\Models\Attribute;
use App\Models\Size;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;
use Spatie\Translatable\HasTranslations;

class CategoryProduct extends Model
{
    use Mediable;
    use HasTranslations;
    public $translatable = ['name','desc'];
    public function products(){
        return $this->hasMany(Product::class,'product_cat_id');
    }

    public function children() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }
    public function attributes() {
        return $this->hasMany(Size::class, 'category_id', 'id');
    }

    public function parent() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }
    public function users()
    {
        return $this->belongsToMany(Admin::class, 'user_categories','category_id','user_id');
    }
    protected $table = 'category_products';



}
