<?php

namespace App\Models;

use App\CategoryProduct;
use App\Product;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Size extends Model
{
//    use HasTranslations;
//    public $translatable = ['name'];
    protected $fillable = ['name','category_id'];

    public function category (){
        return $this->belongsTo(CategoryProduct::class,'category_id');
    }
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_sizes','size_id','product_id')->withPivot('quantity');
    }
}
