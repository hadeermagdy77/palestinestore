<?php

namespace App;

use App\Models\ProductAttribute;
use App\Models\ProductVariation;
use App\Models\Size;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
use Plank\Mediable\Mediable;
use Spatie\Translatable\HasTranslations;
use Illuminate\Support\Carbon;

class Product extends Model
{
    use Mediable;
    use HasTranslations;
    public $translatable = ['name', 'desc'];
    protected $fillable =['slug'];

    public static function sortBy(string $string, string $string1)
    {
    }

    public function setSlugAttribute($value){
        $this->attributes['slug'] = Str::Slug($value);
    }

    public function categoryProduct()
    {
        return $this->belongsTo(CategoryProduct::class, 'product_cat_id');
    }
    public function parentCategory()
    {
        return $this->belongsTo(CategoryProduct::class, 'parent_cat_id');
    }
//    public function brand(){
//        return $this->belongsTo(Partner::class, 'partner_id');
//
//    }
    public function countries()
    {
        return $this->belongsToMany(Country::class, 'product_countries','product_id','country_id');
    }
    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'product_sizes','product_id','size_id')->withPivot('quantity');
    }


    public function variations()
    {
        return $this->hasMany(ProductVariation::class, 'product_id');
    }

    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class, 'product_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_products','product_id','order_id');
    }
    public function getDiscountAttribute()
    {
        if ($this->start_date < Carbon::now()->format('Y-m-d H:i:s') && $this->end_date > Carbon::now()->format('Y-m-d H:i:s')) {
            return $this->discountPrice;
        } else {
            return 0;
        }
    }
}
