<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status')->default('new');
            $table->boolean('views')->default(0);
            $table->string('firstName');
            $table->string('lastName')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('company')->nullable();
            $table->string('city')->nullable();
            $table->string('apartment')->nullable();
            $table->integer('zip')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('note')->nullable();
            $table->dateTime('delivered_at')->nullable();
            $table->float('subtotal')->nullable();
            $table->float('shipping')->nullable();
            $table->float('total')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
