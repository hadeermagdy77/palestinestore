<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        setting()->set([
            'general.logo' => '',
            'general.website_name' => 'ecommerce',
            'general.tags' => '',
            'general.map_iframe_link'=>'',
            'general.address' => '',
            'general.shipping' => '20',
            'general.phone_number' => '',
            'general.contact_email' => '',
            'general.facebook_page_link' => '',
            'general.twitter_page_link' => '',
            'general.whatsapp_number' => '',
            'general.instagram_page_link' => '',
            'general.google-plus_page_link'=>'',
            'general.youTube_page_link' => '',
            'general.website_description'=>'',
            'general.navbar_background_color'=>'',
            'general.navbar_text_color'=>'',
            'general.navbar_hover_color'=>'',
            'general.footer_background_color'=>'',
            'general.footer_text_color'=>'',
            'general.footer_bottom_border_color'=>'',
        ]);

        setting()->save();
    }
}
