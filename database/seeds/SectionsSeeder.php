<?php

use App\Section;
use Illuminate\Database\Seeder;

class SectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $NewReleases = Section::Create([
            'name' => 'New Releases',
            'url' => 'newReleases',
            'description' => '',
            'type' => 'newReleases',
            'order' => '1',
            'active' => '1',
        ]);

        $NewReleases->save();

        $Brands = Section::Create([
            'name' => 'Brands',
            'url' => 'brands',
            'type' => 'brands',
            'order' => '2',
            'active' => '1',
        ]);

        $Brands->save();

        $catProduct = Section::Create([
            'name' => 'categories',
            'url' => 'categories',
            'description' => '',
            'type' => 'catProduct',
            'order' => '3',
            'active' => '1',
        ]);

        $catProduct->save();
        $sale = Section::Create([
            'name' => 'Sale',
            'url' => 'sale',
            'description' => '',
            'type' => 'sale',
            'order' => '4',
            'active' => '1',
        ]);

        $sale->save();

        $blog = Section::Create([
            'name' => 'blog',
            'url' => 'blog',
            'description' => '',
            'type' => 'blog',
            'order' => '5',
            'active' => '1',
        ]);

        $blog->save();

        $instagram = Section::Create([
            'name' => 'instagram',
            'url' => 'instagram',
            'description' => '',
            'type' => 'instagram',
            'order' => '6',
            'active' => '1',
        ]);

        $instagram->save();
        $twitter = Section::Create([
            'name' => 'twitter',
            'url' => 'twitter',
            'description' => '',
            'type' => 'twitter',
            'order' => '7',
            'active' => '1',
        ]);
        $twitter->save();

    }

}
