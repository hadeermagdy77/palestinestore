@extends('admin.layout.master')
@section('title','Instagram')
@section('content')
    <div class="row row-card-no-pd">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">{{__('Instagram')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive table-hover table-sales">
                                <table class="table">
                                    <thead>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Link')}}</th>
                                    <th>{{__('Action')}}</th>
                                    </thead>
                                    @if($instagrams->count() > 0)
                                        @foreach($instagrams as $instagram)
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @if($instagram->hasMedia('instagram'))
                                                        <div class="flag">
                                                            <img width="80" height="80" src="{{ $instagram->lastMedia('instagram')->getUrl() }}">
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>{{$instagram->link}}</td>

                                                <td>

                                                    <a href="{{route('admin.instagrams.edit',$instagram->id)}}" class="btn btn-info btn-xs">
                                                        <i class='fa fa-edit'></i>{{__('Edit')}}
                                                    </a>

                                                    <button class="btn btn-danger btn-xs" type="submit" onclick="removeInstagram('{{route('admin.instagram.destroy',$instagram->id)}}')">
                                                        <i class="fas fa-trash"></i> {{__('Delete')}}
                                                    </button>
                                                </td>
                                            </tr>

                                            </tbody>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script>
        function removeInstagram(url, e) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = url;
                    }
                });
        };

    </script>
@stop
