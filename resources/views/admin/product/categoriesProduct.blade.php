@extends('admin.layout.master')
@section('title','categories Product')
@section('content')
    <div class="row row-card-no-pd">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">{{__('Product Category')}}</h4>
                    </div>

                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#productCategory">
                        {{__('Add Product Category')}}
                    </button>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive table-hover table-sales">
                                <table class="table">
                                    <thead>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Description')}}</th>
                                    <th>{{__('Action')}}</th>
                                    </thead>
                                    @if($categoriesProduct->count() > 0)
                                        @foreach($categoriesProduct as $categoryProduct)
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @if($categoryProduct->hasMedia('catproduct'))
                                                        <div class="flag">
                                                            <img width="80" height="80" src="{{ $categoryProduct->lastMedia('catproduct')->getUrl() }}">
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>{{$categoryProduct->name}}</td>
                                                <td>
                                                    {{$categoryProduct->desc}}
                                                </td>
                                                <td>
                                                    <a href="#" onclick="editCategory('{{$categoryProduct->name }}','{{$categoryProduct->desc }}','{{$categoryProduct->lastMedia('catproduct')->getUrl()}}','{{route('admin.categoryProduct.update',$categoryProduct->id)}}')" class="btn btn-info btn-xs" data-toggle="modal" data-target="#updateProductCategory">
                                                        <i class='fa fa-edit'></i>{{__('Edit')}}
                                                    </a>


                                                        <button class="btn btn-danger btn-xs" type="submit" onclick="removeCat('{{$categoryProduct->name}}','{{route('admin.catProduct.destroy',$categoryProduct->id)}}')">
                                                            <i class="fas fa-trash"></i> {{__('Delete')}}
                                                        </button>


                                                </td>
                                            </tr>

                                            </tbody>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <div class="modal fade" id="productCategory" tabindex="-1" role="dialog" aria-labelledby="productCategory" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="BlogCategory">{{__('Add Product Category')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form  method="POST" action="{{route('admin.categoryProduct.store')}}" enctype="multipart/form-data" file="true">
                        @csrf

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="desc"> Description</label>
                                <input type="text" cols="10" rows="5" maxlength="120"  name="desc" class="form-control" >
                            </div>

                        <div class="form-group">
                            <label for="img">{{__('Upload Image')}}</label>
                            <input type="file" name="img" class="form-control-file" id="img" required>
                        </div>
                        <div class="form-group" id="imgPreview" >
                            <img src="" class="img-fluid" alt="" width="300px" height="300px">
                        </div>

                        <button type="submit" class="btn btn-primary float-right">{{__('Save')}}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateProductCategory" tabindex="-1" role="dialog" aria-labelledby="updateProductCategory" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateBlogCategory">{{__('update Product Category')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form  method="POST" action="" enctype="multipart/form-data" file="true">
                        @csrf
                        @method('PUT')

                            <div class="form-group">
                                <label for="name"> Name</label>
                                <input type="text" name="name" class="form-control" value="">
                            </div>

                            <div class="form-group">
                                <label for="desc"> Description</label>
                                <input type="text" cols="10" rows="5" maxlength="120" name="desc" class="form-control" value=""/>
                            </div>

                        <div class="form-group">
                            <label for="img">{{__('Upload Image')}}</label>
                            <input type="file" name="img" class="form-control-file" id="img" >
                        </div>
                        <div class="form-group" id="imgPreview" >
                            <img src="" class="img-fluid" alt="" width="300px" height="300px">
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Save</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $('input[type="file"]').on('change', function (e) {
            let fileName = e.target.files[0].name,
                reader = new FileReader();

            $(e.target).siblings('label').text(fileName);

            reader.onload = function (event) {
                $('#imgPreview img').attr('src', event.target.result);
            };

            reader.readAsDataURL(e.target.files[0]);
            $('#imgPreview').show();
        });

        function editCategory(name,desc,img,href) {
            // alert(href);
            let modal = $('#updateProductCategory');
            modal.find('.modal-body input[name="name"]').val(name);
            modal.find('.modal-body input[name="desc"]').val(desc);
            modal.find('.modal-body img').attr('src', img);
            // modal.find('.modal-body select[name="parentCategory"]').val(parent);

            modal.find('.modal-body form').attr("action",href);

        };
        function removeCat(name, url, e) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = url;
                    }
                });
        };
    </script>
@stop
