@extends('admin.layout.master')
@section('title','Create')
@section('style')
    <style>

        * {
            box-sizing: border-box;
        }

        .container {
            margin: 0 auto;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 100%;
        }

        .internal {
            max-width: 900px;
            background: #ffffff;
            padding: 10px;
            margin: 0 auto;
            border-radius: 3px;
        }

        .l-float-left {
            float: left;
        }

        .l-float-right {
            float: right;
        }

        .l-inline-block {
            display: inline-block;
        }


        .form__row {
            float: left;
            width: 100%;
            margin-bottom: 12px;
        }


        .settings__title--inline {
            font-size: 1.2em;
            margin-bottom: 12px;
        }
        .text-color--primaryBlue {
            color: #0788ca;
        }
        .l-cursor-pointer {
            cursor: pointer;
        }
        .form-multicheckbox {
            float: left;
            position: relative;
            padding: 10px;
        }
        .active .shipping-countries__content {
            display: block;
        }
        .active .form-multicheckbox__container {
            display: block;
        }
        .active .form-multicheckbox__name {
            border-radius: 2px 2px 0px 0px;
        }
        /*.form-multicheckbox__container {*/
        /*    display: block;*/
        /*}*/

        .form-multicheckbox > input[type="checkbox"] {
            float: left;
            margin-right: 19px;
            margin-top: 10px;
        }
        .form-multicheckbox__name {
            background-repeat: no-repeat;
            background-position: 95% center;
            background-position: right 12px center, left center;
            padding: 5px 12px;
            border: 1px solid #cccccc;
            border-radius: 2px;
            float: left;
            width: calc(100% - 40px);
        }
        .form-multicheckbox__container {
            display: none;
            float: left;
            width: calc(100% - 40px);
            position: relative;
            left: 31px;
            top: 26px;
            background: #ffffff;
            border: 1px solid #cccccc;
            border-top: 0px;
            border-radius: 0px 0px 2px 2px;
            padding: 12px;
            height: 155px;
            overflow-y: scroll;
            box-shadow: 0px 2px 5px rgba(1, 1, 1, 0.2);
            z-index: 999;
        }
        .ie-9  {
            left: 50px;
            width: 313px;
        }
        .ie10  {
            left: 50px;
        }

        .form-multicheckbox__priority {
            border-bottom: 1px solid #cccccc;
            padding-bottom: 6px;
            margin-bottom: 12px;
        }
        .form-multicheckbox__count {
            padding-left: 0.5em;
        }
        .form-multicheckbox__row {
            margin-bottom: 6px;
        }


        .shipping-countries {
            position: relative;
            padding-left: 0px;
        }

        .shipping-countries__content {
            display: block;
        }

        .shipping-countries__count {
            font-weight: normal;
            color: #5c258d;
        }
        .shipping-countries__content {
            display: none;
            position: absolute;
            z-index: 9999;
            width: 200px;
            right: 0px;
            top: 2.2em;
            padding: 12px;
            background: #ffffff;
            border: 1px solid #cccccc;
            border-radius: 2px;
            box-shadow: 0px 2px 5px rgba(1, 1, 1, 0.2);
            color: #1a1a1a;
            font-weight: normal;
            max-height: 155px;
            overflow-y: scroll;
        }
        .shipping-countries div {
            margin-bottom: 6px;
        }
        .shipping-countries:last-child {
            margin-bottom: 0px;
        }


    </style>
@stop
@section('content')
    <div class="page-inner">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{route('admin.products.store')}}"  enctype="multipart/form-data" accept-charset="utf-8" file="true">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">{{__('Add Product')}}</div>
                            <div class="form-group">
                                <input type="hidden" name="type" value="post">
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="category">{{ __('Select Category') }}*</label>
                                        <select name="category" id="category"
                                                class="custom-select form-control auto-save"  data-parsley-group="order" data-parsley-required>
                                            <option value="">{{ __('Select') }} {{ __('Category') }}</option>
                                            @foreach($categoriesProduct as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-2">
                                    <div class="form-group">
                                        <label for="brand">{{ __('Brand') }}*</label>
                                        <select name="partner_id" id="brand" class="custom-select auto-save" >
                                            <option value="">{{ __('Select') }} {{ __('Brand') }}</option>
                                            @foreach($brands as $brand)
                                                <option value="{{ $brand->id }}">{{ $brand->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name"> Name *</label>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{old('name')}}" required>
                                        </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="price">{{ __('Price') }}*</label>
                                        <input type="text" name="price" class="form-control" id="price" value="{{old('price')}}" placeholder="Enter price" >
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="discount">{{ __('Sale Price') }}</label>
                                        <input type="text" name="discountPrice" class="form-control" id="discount" value="{{old('discountPrice')}}" placeholder="Enter discount" >
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="start_date">{{ __('Sale Start Date') }}</label>
                                        <input type="date" name="start_date" class="form-control" id="start" value="{{old('start_date')}}" placeholder="Enter start_date" >
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="end_date">{{ __('Sale End Date') }}</label>
                                        <input type="date" name="end_date" class="form-control" id="end_date" value="{{old('end_date')}}" placeholder="Enter end_date" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="desc"> Full Description</label>
                                        <textarea class="form-control summernote" id="summernote" value="{{old('desc')}}" name="desc" id="desc" ></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-10">
                                    <div class="form-group">
                                        <label for="img">{{__('Upload Image')}}</label>
                                        <input type="file" name="img[]" class="form-control-file" id="img" multiple required>
                                    </div>
                                    <div class="form-group" id="imgPreview" >
                                        <img src="" class="img-fluid" alt="" width="300px" height="300px">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">  {{ __('Sizes & Quantities')}}</div>
                        </div>

                        <div class="card-body">
                            <div class="wizard-container">

                                <div class="form-group row">

                                    <label for="inlineFormInputGroupAttributes" class="col-sm-2 col-form-label">{{ __('Sizes') }}</label>
                                    <div class="col-sm-8">
                                        <select name="selectAttribute" class="custom-select form-control" id="selectAttribute" >
                                            <option value="">{{ __('Select Size') }}</option>
                                            {{--                                        @foreach($attributes as $attribute)--}}
                                            {{--                                            <option value="{{$attribute->name}}">{{ $attribute->name }}</option>--}}
                                            {{--                                            @endforeach--}}
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group row ">
                                    <div class="col-md-2">

                                    </div>
                                    <div class="col-md-8">
                                        <table id="attributes" class="table">
                                            <thead>
                                            <tr>
                                                <th>{{__('Size')}}</th>
                                                <th>{{__('Quantity')}}</th>
                                                <th>{{__('Delete')}}</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"
                                           for="quantity">{{ __('Quantity') }}</label>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="quantity" value="{{old('quantity')}}" name="quantity"
                                                   required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">{{__('Shipping Details')}}</div>
                        </div>

                        <div class="card-body">

                            <div class="container">
                                <div class="internal">
                                    <div class="form__row">
                                        <div class="l-float-left">
                                            <div class="settings__title--inline" >{{__('Countries')}}</div>
                                            <span class="text-color--primaryBlue l-pad-gutter-rhythm--half js-shipping-select-all l-cursor-pointer" >{{__('Select all')}}</span> |
                                            <span class="text-color--primaryBlue l-pad-gutter-rhythm--half js-shipping-deselect-all l-cursor-pointer">{{__('Deselect all')}}</span>
                                        </div>
                                        <div class="l-float-right">
                                            {{__('I ship to')}}
                                            <div class="l-inline-block shipping-countries select clean text capitalize">
                                                <div class="shipping-countries__count l-cursor-pointer">
                                                    <span>0</span> {{__('Countries')}} &nbsp;<i class="fa fa-caret-down"></i>
                                                </div>
                                                <div class="shipping-countries__content">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__row">
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('Africa')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','Africa') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" id="{{$country->id}}" data-name="{{$country->name}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('North America')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','North America') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" id="{{$country->id}}" data-name="{{$country->name}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('Asia')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','Asia') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" id="{{$country->id}}" data-name="{{$country->name}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('Antarctica')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','Antarctica') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" data-name="{{$country->name}}" id="{{$country->id}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('Europe')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','Europe') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" data-name="{{$country->name}}" id="{{$country->id}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('Australia')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','Oceania') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" data-name="{{$country->name}}" id="{{$country->id}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-4 form-multicheckbox l-vertical-rhythm-bottom--half">
                                            <input type="checkbox" name="north-america" class="form-multicheckbox__main-check" />
                                            <div class="form-multicheckbox__name">{{__('South America')}} <span class="form-multicheckbox__count"></span>

                                            </div>
                                            <div class="form-multicheckbox__container">
                                                @foreach($countries->where('continent','South America') as $country)
                                                    <div class="form-multicheckbox__row">
                                                        <input type="checkbox" name="countries[]" data-name="{{$country->name}}" id="{{$country->id}}" value="{{$country->id}}" />
                                                        <label for="{{$country->id}}">{{$country->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button type="submit" class="btn btn-success float-right">{{__('Save')}}</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="productCategory" tabindex="-1" role="dialog" aria-labelledby="productCategory" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="BlogCategory">{{__('Add Product Category')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form  method="POST" action="{{route('admin.categoryProduct.store')}}" enctype="multipart/form-data" file="true">
                        @csrf
                        <div class="form-group">
                            <label for="parentCategory">{{ __('parent Category') }}</label>
                            <select class="form-control" name="parentCategory">
                                <option value="">{{__('parent Category')}}</option>
                                @foreach($categoriesProduct->where('parent_id',null) as $categoryProduct)
                                    <option value="{{$categoryProduct->id}}" name="parentCategory" >{{$categoryProduct->name}}</option>
                                @endforeach
                            </select>

                        </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" >
                            </div>

                            <div class="form-group">
                                <label for="desc">Description</label>
                                <input type="text" cols="10" rows="5" maxlength="120"  name="desc" required class="form-control" >
                            </div>
                        <div class="form-group">
                            <label for="img">{{__('Upload Image')}}</label>
                            <input type="file" name="img" class="form-control-file" id="img" required>
                        </div>
                        <div class="form-group" id="imgPreview" >
                            <img src="" class="img-fluid" alt="" width="300px" height="300px">
                        </div>

                        <button type="submit" class="btn btn-primary float-right">{{__('Save')}}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script>
        let  index = 0, indexAttr = 1, values = [], valuesPaired = [];

        $(document).on('change','#img', function (e) {
            let x;
            // console.log($(n));
            for( x = 0 ;x<$(e.target.files).length; x++ ){
                let reader = new FileReader();
                // var name = document.createElement('p');
                let name =e.target.files[x].name;
                // $('#imgPreview').append(name);
                var l = document.getElementsByClassName('img-fluid');
                $(l).remove();
                reader.onload = function (event) {
                    let target = $('#imgPreview');
                    target.append(' <div style="display: inline-block;"><img name='+name+' src='+event.target.result+' width="100px" style="margin: 10px;"  id="image" class="img-fluid">'
                        );
                };
                reader.readAsDataURL(e.target.files[x]);
                $('#imgPreview').show();
            }
        });

        $(document).ready(function() {
            $('.summernote').summernote({
                tabsize: 2,
                height: 150,
                minHeight: null,
                maxHeight: null,
                focus: false,
                lang: '{{ config("app.locale") }}',
                toolbar: [
                    // [groupName, [list of button]]
                    ['font', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['style', 'ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['insert', [ 'table','hr','video','link']],
                    ['custom', ['picture']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['misc', ['fullscreen', 'undo', 'redo', 'help', 'codeview']]
                ],
            });
            $('select[name="category"]').on('change', function () {
                var categoryID = $(this).val();
                if (categoryID) {

                    let ur2 = '{{url('/admin/categories/attributes')}}';

                    $.ajax({
                        url: ur2 + '/' + categoryID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="selectAttribute"]').empty();
                            $('select[name="selectAttribute"]').append('<option value="">{{__('Select Size')}}</option>');
                            $.each(data, function (key, value) {
                                $('select[name="selectAttribute"]').append('<option data-id="'+key+'" value="' + value + '">' + value + '</option>');
                            });
                        }
                    });

                }
            });
        });
        function checkVal(sel) {
            if (sel.value === "new") {
                $('.addNewAttribute').removeAttr('disabled');
            } else if (sel.value === "select") {
                $('.addNewAttribute').attr('disabled', 'disabled');
            } else {

            }
        }
        $('#selectAttribute').change(function () {

            $('#selectAttribute option:selected').hide();
            let id = $('#selectAttribute option:selected').data('id');
            let atrrName = $('#selectAttribute').val();
            // console.log(atrrName);
            if (atrrName) {
                atrrName2 = atrrName.replace(' ', '');
                $('#attributes tbody').append('<tr><td><input type="hidden" name="attributes[' + index + '][id]" value="' + id + '" ><input type="text" class="form-control" name="attributes[' + index + '][name]" value="' + atrrName2 + '" readonly></td><td><input name="attributes[' + index + '][quantity]" type="number" class="form-control attributesSelect"></td><td><button type="button" class="btn btn-link" data-name="'+atrrName+'"  onclick="removeAttr(this);">' + "{{ __('Delete') }}" + '</button></td></tr>');

                index++;
            }
            let $quantity = $('#quantity');
            $quantity.removeAttr("required");
            $quantity.attr("disabled", true);
        });
        function removeAttr(el) {

            if (confirm("Are you sure?")) {
                let name  =  $(el).data('name');
                if (($(el).parents('tr').parents('tbody').children().length) <= 3) {
                    let $quantity = $('#quantity');
                    $quantity.removeAttr("disabled");
                    $quantity.attr("required", true);
                }
                ;
                $(el).parents('tr').remove();
                console.log(name);
                $('#selectAttribute  option[value="'+name+'"]').show();

            }
            return false;
        }
        function removeVar(el) {

            if (confirm("Are you sure?")) {
                if (($(el).parents('tr').parents('tbody').children().length) <= 3) {
                    let $quantity = $('#quantity');
                    $quantity.removeAttr("disabled");
                    $quantity.attr("required", true);
                }
                ;
                $(el).parents('tr').remove();
            }
            return false;
        }

        $(document).ready(function () {
            $('button[type="submit"]').on('click', function() {
                // skipping validation part mentioned above


                let cbx_group = $("input:checkbox[name='countries[]']");
                cbx_group.prop('required', true);
                if(cbx_group.is(":checked")){
                    cbx_group.prop('required', false);
                }else {

                    $('.settings__title--inline').append(' <br><span style="color: red" ">Please Select Shipping Country</span>\n <br> ' );
                }
            });
        });
        // ***
        // Update ship to count on shipping locations
        // whenever called; count checked inputs and apply to the count.
        // also; enter checked input's value into an array.
        // join array and put contents into a drop down.
        //
        var updateShipToCountries = function() {
            var target = $('.shipping-countries');
            var targetCount = target.find('.shipping-countries__count span');
            var targetContent = target.find('.shipping-countries__content');
            var checkedInputs = $('.form-multicheckbox__container input:checked');
            var countries = [];

            checkedInputs.each(function() {
                var value = $(this).data('name');
                value = value.replace(/-/g, ' ');

                countries.push('<div class="">' + value + '</div>');
            });
            targetCount.html(countries.length);
            targetContent.html(countries.join(''));
        }

        // ***
        // Shipping locations continent checkbox trigger
        // on click find continent's related .form-multicheckbox__container.
        // change __container's children inputs to be checked if main-check is checked.
        // update continents counter span to how many children have been checked.
        // if main-check is unchecked, change __container's inputs to unchecked.
        // reset continents counter span to 0 when no inputs are checked.
        // trigger updateShipToCountries to update the count.
        //
        $('.form-multicheckbox__main-check').on('click', function() {
            var _this = $(this);
            var parent = _this.closest('.form-multicheckbox');

            if (_this.prop('checked')) {
                var countCheckboxes = parent.find('.form-multicheckbox__container input').length;
                parent.find('.form-multicheckbox__container input').each(function() {
                    $(this).prop('checked', true);
                    parent.find('.form-multicheckbox__name span').html('(' + countCheckboxes + ')');
                });
            } else {
                parent.find('.form-multicheckbox__container input').each(function() {
                    $(this).prop('checked', false);
                    parent.find('.form-multicheckbox__name span').html('');
                });
            }
            updateShipToCountries();
        });

        // ***
        // Update continent's checked count and adjust main-check status
        // on click, count how many children inputs have been checked.
        // if all children are clicked, update the count and turn main check checked.
        // if any children are clicked, update the count and turn main check indeterminate.
        // if no children are clicked, clear the count and remove check/indeterminate from main check.
        // trigger updateShipToCountries to update the country count.
        //
        $('.form-multicheckbox__container input').on('click', function() {
            var parent = $(this).closest('.form-multicheckbox');
            var countChecked = parent.find('.form-multicheckbox__container input:checked').length;
            var countInputs = parent.find('.form-multicheckbox__container input').length;
            var checkedText = countChecked > 0 ? '(' + countChecked + ')' : '';

            if (countInputs === countChecked) {
                parent.find('.form-multicheckbox__main-check').prop('checked', true);
                parent.find('.form-multicheckbox__main-check').prop('indeterminate', false);
            } else if (countChecked > 0) {
                parent.find('.form-multicheckbox__main-check').prop('indeterminate', true);
            } else {
                parent.find('.form-multicheckbox__main-check').prop('indeterminate', false);
            }
            parent.find('.form-multicheckbox__name span').html(checkedText);
            updateShipToCountries();
        });

        // ***
        // Show multicheckbox drop down
        // on click, show clicked drop down.
        // hide other multicheckbox drop downs.
        //
        $('.form-multicheckbox__name').on('click', function() {
            var parent = $(this).closest('.form-multicheckbox');
            var labels = parent.find('.form-multicheckbox__container label');
            var firstLetters = [];

            labels.each(function() {
                var _this = $(this);
                var html = _this.html().toLowerCase()[0];

                if (!firstLetters[html]) {
                    firstLetters[html] = _this;
                }
            });

            $('.form-multicheckbox').not(parent).removeClass('active');
            parent.toggleClass('active');

            $('body').on('keypress', function(e) {
                var keyed = e.key;
                var target = firstLetters[keyed][0].offsetTop;
                var targetPosition = target;

                parent.find('.form-multicheckbox__container').scrollTop(targetPosition);
            });
        });

        // ***
        // Show shipping countries drop down
        // on click, if any shipping countries present (div's) toggle active class.
        //
        $('.shipping-countries').on('click', function() {
            var _this = $(this);

            if (_this.find('.shipping-countries__content div').length > 0) {
                _this.toggleClass('active');
            }
        });

        // ***
        // Select all shipping countries
        // on click, change all checkboxes to checked.
        // update each continent's count to their amount of child inputs (checked).
        // trigger updateShipToCountries to update the country count.
        //
        $('.js-shipping-select-all').on('click', function() {
            $('.form-multicheckbox input').prop('checked', true);
            $('.form-multicheckbox input').prop('indeterminate', false);

            $('.form-multicheckbox').each(function() {
                $(this).find('.form-multicheckbox__count').html('(' + $(this).find('input').length + ')')
            });
            updateShipToCountries();
        });

        // ***
        // Deselect all shipping countries
        // on click, change all checkboxes to unchecked.
        // clear each continent's count.
        // trigger updateShipToCountries to update the country count.
        //
        $('.js-shipping-deselect-all').on('click', function() {
            $('.form-multicheckbox input').prop('indeterminate', false);
            $('.form-multicheckbox input').prop('checked', false);
            $('.form-multicheckbox span').html('');
            updateShipToCountries();
        });

    </script>
@stop
