<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive table-hover table-sales">
                <table id="table" class="table">
                    <thead>

                    <th>{{__('First Name')}}</th>
                    <th>{{__('Last Name')}}</th>
                    <th>{{__('Email')}}</th>
                    <th>{{__('Phone')}}</th>
                    <th>{{__('Country')}}</th>
                    <th>{{__('City')}}</th>
                    <th>{{__('Address')}}</th>
                    <th>{{__('Apartment')}}</th>
                    <th>{{__('Zip')}}</th>
{{--                    <th>{{__('Note')}}</th>--}}

                    </thead>
                        <tbody>

                            <tr>
                                <td>{{$order->firstName}}</td>
                                <td>{{$order->lastName}}</td>
                                <td>{{$order->email}}</td>
                                <td>{{$order->phone}}</td>
                                <td>{{$order->country->name}}</td>
                                <td>{{$order->city}}</td>
                                <td>{{$order->address}}</td>
                                <td>{{$order->apartment}}</td>
                                <td>{{$order->zip}}</td>
                            </tr>
                        </tbody>

                </table>

            </div>
        </div>

    </div><div class="row">
        <div class="col-md-12">
            <div class="table-responsive table-hover table-sales">
                <table id="table" class="table">
                    <thead>

                    <th>{{__('Product')}}</th>
                    <th>{{__('Quantity')}}</th>
                    <th>{{__('Price')}}</th>

                    </thead>
                        <tbody>
                        @foreach($order->products as $product)
{{--                            @dd($product)--}}
                            <tr>
                                <td>{{$product->name}}  @if($product->sizes->count() > 0)
                                        ({{$product->sizes->where('id',$product->pivot->product_variation_id)->first()->name}} )
                                    @endif
                                </td>
                                <td>
                                    {{$product->pivot->quantity}}
                                </td>
                                <td>
                                    {{$product->pivot->price}}
                                </td>


                            </tr>


                        @endforeach
                        <tr>
                            <td></td>
                            <td ><h3>{{__('SubTotal')}}</h3></td>
                            <td>
                                {{$order->subtotal}}
                            </td>
                        </tr><tr>
                            <td></td>
                            <td ><h3>{{__('Shipping')}}</h3></td>
                            <td>
                                {{$order->shipping}}
                            </td>
                        </tr><tr>
                            <td></td>
                            <td ><h3>{{__('Total')}}</h3></td>
                            <td>
                                {{$order->total}}
                            </td>
                        </tr>

                        </tbody>

                </table>

            </div>
        </div>

    </div>
</div>
