@extends('admin.layout.master')
@section('title','Twitter')
@section('content')
    <div class="row row-card-no-pd">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">{{__('Twitter')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive table-hover table-sales">
                                <table class="table">
                                    <thead>
                                    <th>{{__('Title')}}</th>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Action')}}</th>
                                    </thead>
                                    @if($twitters->count() > 0)
                                        @foreach($twitters as $twitter)
                                            <tbody>
                                            <tr>

                                                <td>
                                                    @if($twitter->hasMedia('twitter'))
                                                        <div class="flag">
                                                            <img width="80" height="80" src="{{ $twitter->lastMedia('twitter')->getUrl() }}">
                                                        </div>
                                                    @endif
                                                </td>

                                                <td>{{$twitter->name}}</td>
                                                <td>

                                                    <a href="{{route('admin.twitters.edit',$twitter->id)}}" class="btn btn-info btn-xs">
                                                        <i class='fa fa-edit'></i>{{__('Edit')}}
                                                    </a>

                                                    <button class="btn btn-danger btn-xs" type="submit" onclick="removeTwitter('{{$twitter->name}}','{{route('admin.twitter.destroy',$twitter->id)}}')">
                                                        <i class="fas fa-trash"></i> {{__('Delete')}}
                                                    </button>
                                                </td>
                                            </tr>

                                            </tbody>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script>
        function removeTwitter(name,url, e) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = url;
                    }
                });
        };

    </script>
@stop
