@extends('admin.layout.master')
@section('title','Update Instagram Images')
@section('content')
    <div class="page-inner">

        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{route('admin.twitters.update',$twitter->id)}}" enctype="multipart/form-data" file="true">
                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">{{__('Updata twitter Page')}}</div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="link">{{ __('Title') }}</label>
                                        <input  class="form-control" name="name" id="name" value="{{$twitter->name}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <label for="link">{{ __('Twitter Link') }}</label>
                                        <input  class="form-control" name="twitterLink" id="link" value="{{$twitter->twitterLink}}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="img">{{__('Upload Image')}}</label>
                                        <input type="file" name="img[]"  id="img" multiple >
                                    </div>
                                    <div class="form-group" id="imgPreview" >

                                    </div>
                                    <div class="form-group" id="imgPreview2">

                                    @if($twitter->hasMedia('twitter'))
                                        @foreach($twitter->getMedia('twitter') as $image)
                                            <div style="display: inline-block;">
                                                <img  src="{{$image->getUrl()}}"  name="{{$image->getBasenameAttribute()}}" width="100px" height="100px">
                                                <a href="{{route('admin.twitter.deleteImage',$image->id)}}"><i class="fa fa-trash "></i></a>

                                            </div>
                                        @endforeach
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button type="submit" class="btn btn-success float-right">{{__('Update')}}</button>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@stop
@section('script')
    <script>

        let  index = 10, indexAttr = 1, values = [], valuesPaired = [];

        $(document).ready(function() {
            $('.summernote').summernote({
                tabsize: 2,
                height: 300,
                minHeight: null,
                maxHeight: null,
                focus: false,
                lang: '{{ config("app.locale") }}',
                toolbar: [
                    // [groupName, [list of button]]
                    ['font', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['style', 'ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['insert', [ 'table','hr','video','link']],
                    ['custom', ['picture']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['misc', ['fullscreen', 'undo', 'redo', 'help', 'codeview']]
                ],
            });
            $('select[name="category"]').on('change', function () {
                var categoryID = $(this).val();
                if (categoryID) {

                    let ur2 = '{{url('/admin/categories/attributes')}}';

                    $.ajax({
                        url: ur2 + '/' + categoryID,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="selectAttribute"]').empty();
                            $('select[name="selectAttribute"]').append('<option value="">{{__('Select Attribute')}}</option>');
                            $.each(data, function (key, value) {
                                $('select[name="selectAttribute"]').append('<option data-id="'+key+'" value="' + value + '">' + value + '</option>');
                            });
                        }
                    });

                } else {
                    $('select[name="subCategory"]').empty();
                }
            });

        });
        function removeItem(item) {
            let parent = $(item.target.parentElement);
            let val =$(item.target.parentElement).children('img').attr('name');

            var input=$('input[type = "file"]')[0];
            for (let i = 0 ; i < $('input[type = "file"]')[0].files.length;i++ ) {
                const dt = new DataTransfer()
                for (let file of input.files)
                {
                    console.log(file['name']);
                    if (file['name'] !== val) {
                        dt.items.add(file);
                    }
                }
                input.files = dt.files;
            }
            // console.log( input.files);
            parent.remove();
        }
        $(document).on('change','#img', function (e) {
            let x;

            // console.log($(n));
            var i = document.getElementsByClassName('delete');
            $(i).remove();
            for( x = 0 ;x<$(e.target.files).length; x++ ){
                let reader = new FileReader();


                // var name = document.createElement('p');
                let name =e.target.files[x].name;
                // $('#imgPreview').append(name);
                var l = document.getElementsByClassName('img-fluid');
                $(l).remove();
                reader.onload = function (event) {

                    let target = $('#imgPreview');

                    target.append(' <div style="display: inline-block;"><img name='+name+' src='+event.target.result+' width="100px" style="margin: 10px;"  id="image" class="img-fluid">' +
                        '<i class="fa fa-trash delete" onclick="removeItem(event);" ></i>');
                };
                reader.readAsDataURL(e.target.files[x]);
                $('#imgPreview').show();
            }
        });

        $('#selectAttribute').change(function () {

            $('#selectAttribute option:selected').hide();
            let id = $('#selectAttribute option:selected').data('id');
            let atrrName = $('#selectAttribute').val();
            // console.log(atrrName);
            if (atrrName) {
                atrrName2 = atrrName.replace(' ', '');
                $('#attributes tbody').append('<tr><td><input type="hidden" name="attributes[' + index + '][id]" value="' + id + '" ><input type="text" class="form-control" name="attributes[' + index + '][name]" value="' + atrrName2 + '" readonly></td><td><input name="attributes[' + index + '][quantity]" type="number" class="form-control attributesSelect"></td><td><button type="button" class="btn btn-link" data-name="'+atrrName+'"  onclick="removeAttr(this);">' + "{{ __('Delete') }}" + '</button></td></tr>');

                index++;
            }
        });
        function removeAttr(el) {

            if (confirm("Are you sure?")) {
                let name  =  $(el).data('name')
                $(el).parents('tr').remove();
                console.log(name);
                $('#selectAttribute  option[value="'+name+'"]').show();

            }
            return false;
        }

        $(document).ready(function () {
            $('button[type="submit"]').on('click', function() {
                // skipping validation part mentioned above


                let cbx_group = $("input:checkbox[name='countries[]']");
                cbx_group.prop('required', true);
                if(cbx_group.is(":checked")){
                    cbx_group.prop('required', false);
                }else {

                    $('.settings__title--inline').append(' <br><span style="color: red" ">Please Select Shipping Country</span>\n <br> ' );
                }
            });
        });
        // ***
        // Update ship to count on shipping locations
        // whenever called; count checked inputs and apply to the count.
        // also; enter checked input's value into an array.
        // join array and put contents into a drop down.
        //
        var updateShipToCountries = function() {
            var target = $('.shipping-countries');
            var targetCount = target.find('.shipping-countries__count span');
            var targetContent = target.find('.shipping-countries__content');
            var checkedInputs = $('.form-multicheckbox__container input:checked');
            var countries = [];

            checkedInputs.each(function() {
                var value = $(this).data('name');
                value = value.replace(/-/g, ' ');

                countries.push('<div class="">' + value + '</div>');
            });
            targetCount.html(countries.length);
            targetContent.html(countries.join(''));
        }

        // ***
        // Shipping locations continent checkbox trigger
        // on click find continent's related .form-multicheckbox__container.
        // change __container's children inputs to be checked if main-check is checked.
        // update continents counter span to how many children have been checked.
        // if main-check is unchecked, change __container's inputs to unchecked.
        // reset continents counter span to 0 when no inputs are checked.
        // trigger updateShipToCountries to update the count.
        //
        $('.form-multicheckbox__main-check').on('click', function() {
            var _this = $(this);
            var parent = _this.closest('.form-multicheckbox');

            if (_this.prop('checked')) {
                var countCheckboxes = parent.find('.form-multicheckbox__container input').length;
                parent.find('.form-multicheckbox__container input').each(function() {
                    $(this).prop('checked', true);
                    parent.find('.form-multicheckbox__name span').html('(' + countCheckboxes + ')');
                });
            } else {
                parent.find('.form-multicheckbox__container input').each(function() {
                    $(this).prop('checked', false);
                    parent.find('.form-multicheckbox__name span').html('');
                });
            }
            updateShipToCountries();
        });

        // ***
        // Update continent's checked count and adjust main-check status
        // on click, count how many children inputs have been checked.
        // if all children are clicked, update the count and turn main check checked.
        // if any children are clicked, update the count and turn main check indeterminate.
        // if no children are clicked, clear the count and remove check/indeterminate from main check.
        // trigger updateShipToCountries to update the country count.
        //
        $('.form-multicheckbox__container input').on('click', function() {
            var parent = $(this).closest('.form-multicheckbox');
            var countChecked = parent.find('.form-multicheckbox__container input:checked').length;
            var countInputs = parent.find('.form-multicheckbox__container input').length;
            var checkedText = countChecked > 0 ? '(' + countChecked + ')' : '';

            if (countInputs === countChecked) {
                parent.find('.form-multicheckbox__main-check').prop('checked', true);
                parent.find('.form-multicheckbox__main-check').prop('indeterminate', false);
            } else if (countChecked > 0) {
                parent.find('.form-multicheckbox__main-check').prop('indeterminate', true);
            } else {
                parent.find('.form-multicheckbox__main-check').prop('indeterminate', false);
            }
            parent.find('.form-multicheckbox__name span').html(checkedText);
            updateShipToCountries();
        });

        // ***
        // Show multicheckbox drop down
        // on click, show clicked drop down.
        // hide other multicheckbox drop downs.
        //
        $('.form-multicheckbox__name').on('click', function() {
            var parent = $(this).closest('.form-multicheckbox');
            var labels = parent.find('.form-multicheckbox__container label');
            var firstLetters = [];

            labels.each(function() {
                var _this = $(this);
                var html = _this.html().toLowerCase()[0];

                if (!firstLetters[html]) {
                    firstLetters[html] = _this;
                }
            });

            $('.form-multicheckbox').not(parent).removeClass('active');
            parent.toggleClass('active');

            $('body').on('keypress', function(e) {
                var keyed = e.key;
                var target = firstLetters[keyed][0].offsetTop;
                var targetPosition = target;

                parent.find('.form-multicheckbox__container').scrollTop(targetPosition);
            });
        });

        // ***
        // Show shipping countries drop down
        // on click, if any shipping countries present (div's) toggle active class.
        //
        $('.shipping-countries').on('click', function() {
            var _this = $(this);

            if (_this.find('.shipping-countries__content div').length > 0) {
                _this.toggleClass('active');
            }
        });

        // ***
        // Select all shipping countries
        // on click, change all checkboxes to checked.
        // update each continent's count to their amount of child inputs (checked).
        // trigger updateShipToCountries to update the country count.
        //
        $('.js-shipping-select-all').on('click', function() {
            $('.form-multicheckbox input').prop('checked', true);
            $('.form-multicheckbox input').prop('indeterminate', false);

            $('.form-multicheckbox').each(function() {
                $(this).find('.form-multicheckbox__count').html('(' + $(this).find('input').length + ')')
            });
            updateShipToCountries();
        });

        // ***
        // Deselect all shipping countries
        // on click, change all checkboxes to unchecked.
        // clear each continent's count.
        // trigger updateShipToCountries to update the country count.
        //
        $('.js-shipping-deselect-all').on('click', function() {
            $('.form-multicheckbox input').prop('indeterminate', false);
            $('.form-multicheckbox input').prop('checked', false);
            $('.form-multicheckbox span').html('');
            updateShipToCountries();
        });
    </script>
@stop

