@extends('admin.layout.master')
{{--@yield('style')--}}
{{--<style>--}}
{{--    button::before, button::after {--}}
{{--        box-sizing: inherit;--}}
{{--        content: '';--}}
{{--        position: absolute;--}}
{{--        width: 100%;--}}
{{--        height: 100%;--}}
{{--    }--}}
{{--    .draw {--}}
{{--        -webkit-transition: color 0.25s;--}}
{{--        transition: color 0.25s;--}}
{{--    }--}}
{{--    .draw::before, .draw::after {--}}
{{--        border: 2px solid transparent;--}}
{{--        width: 0;--}}
{{--        height: 0;--}}
{{--    }--}}
{{--    .draw::before {--}}
{{--        top: 0;--}}
{{--        left: 0;--}}
{{--    }--}}
{{--    .draw::after {--}}
{{--        bottom: 0;--}}
{{--        right: 0;--}}
{{--    }--}}
{{--    .draw:hover {--}}
{{--        color: #60daaa;--}}
{{--    }--}}
{{--    .draw:hover::before, .draw:hover::after {--}}
{{--        width: 100%;--}}
{{--        height: 100%;--}}
{{--    }--}}
{{--    .draw:hover::before {--}}
{{--        border-top-color: #60daaa;--}}
{{--        border-right-color: #60daaa;--}}
{{--        -webkit-transition: width 0.25s ease-out, height 0.25s ease-out 0.25s;--}}
{{--        transition: width 0.25s ease-out, height 0.25s ease-out 0.25s;--}}
{{--    }--}}
{{--    .draw:hover::after {--}}
{{--        border-bottom-color: #60daaa;--}}
{{--        border-left-color: #60daaa;--}}
{{--        -webkit-transition: border-color 0s ease-out 0.5s, width 0.25s ease-out 0.5s, height 0.25s ease-out 0.75s;--}}
{{--        transition: border-color 0s ease-out 0.5s, width 0.25s ease-out 0.5s, height 0.25s ease-out 0.75s;--}}
{{--    }--}}
{{--</style>--}}
{{--@stop--}}
@section('title','Slider')
@section('content')

    <div class="container">

        <div class="row">
            <div class="col-12">
                <div class="bd-example">
                    @if(count($slider)>0)
                        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                @foreach($slider as $slide)
                                    @if($loop->first)
                                        <div class="carousel-item active">
                                            <img src="{{ $slide->firstMedia('slider')->getUrl() }}" height="400px" class="d-block w-100" alt="{{ $slide->firstMedia('slider')->filename }}">
                                            <div class="carousel-caption d-none d-md-block">
                                                <h5>{{ $slide->header }}</h5>
                                                <p>{{ $slide->paragraph }}</p>
                                                <a href="#" class="btn btn-primary " data-toggle="modal" data-target="#sliderModal" data-edit="false">{{__('Upload')}}</a>
                                                <a href="#" class="btn btn-success " onclick='editImage("{{ $slide->header }}","{{ $slide->paragraph }}","{{ $slide->firstMedia('slider')->getUrl()}}","{{ $slide->url}}","{{ $slide->btn_name}}","{{ route('admin.slider.update', $slide) }}", event);' data-toggle="modal" data-target="#editsliderModal" >{{ __('Edit') }}</a>
                                                <button class="btn btn-danger" onclick="removeSlide('{{ $slide->header }}', '{{ route('admin.slider.delete', $slide) }}', event);">{{ __('Delete') }}</button>
                                                @if($slide->url !== null)
                                                    <button class="btn btn-primary">{{$slide->btn_name}}</button>
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <div class="carousel-item">
                                            <img src="{{ $slide->firstMedia('slider')->getUrl()}}" height="400px"  class="d-block w-100" alt="{{ $slide->firstMedia('slider')->filename }}">
                                            <div class="carousel-caption d-none d-md-block">
                                                <h5>{{ $slide->header }}</h5>
                                                <p>{{ $slide->paragraph }}</p>
                                                <a href="#" class="btn btn-primary " data-toggle="modal" data-target="#sliderModal" data-edit="false">{{__('Upload')}}</a>
                                                <a href="#" class="btn btn-success " onclick='editImage("{{ $slide->header }}","{{ $slide->paragraph }}","{{ $slide->firstMedia('slider')->getUrl()}}","{{ $slide->url}}","{{ $slide->btn_name}}","{{ route('admin.slider.update', $slide) }}", event);' data-toggle="modal" data-target="#editsliderModal" >{{ __('Edit') }}</a>
                                                <button class="btn btn-danger" onclick="removeSlide('{{ $slide->header }}', '{{ route('admin.slider.delete', $slide) }}', event);">{{ __('Delete') }}</button>
                                                <!-- REMOVE BUTTON IF NOT FOUND-->
                                                @if($slide->url !== null)
                                                 <button class="btn btn-primary">{{$slide->btn_name}}</button>
                                               @endif
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @else
                        <div class="card bg-dark text-black-50" style="height: 500px;">
                            <img src="{{asset('backend/img/bg-1.jpg')}}" height="500px" class="card-img" >
                            <div class="card-img-overlay">
                                <h5 class="card-title">title</h5>
                                <p class="card-text">paragraph</p>
                                <a href="{{ route('admin.slider.store')}}" class="btn btn-outline-primary big"
                                   data-toggle="modal" data-target="#sliderModal"
                                   data-edit="false" style="border-color:#101CFE ;color:#fff">{{ __('Uplode') }}</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="sliderModal" tabindex="-1" role="dialog" aria-labelledby="sliderModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="sliderModalLabel">{{__('Main slider')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" method="post" action="{{ route('admin.slider.store')}}" file="true">
                        @csrf
                        <input type="hidden" name="type" value="slider">

                            <div class="form-group">
                                <label for="header">{{__('Title')}}</label>
                                <input class="form-control" type="text" name="header" value="" id="header" >
                            </div>

                            <div class="form-group">
                                <label for="paragraph">{{ __('Paragraph') }}</label>
                                <input class="form-control" type="text" name="paragraph" value="" id="paragraph">
                            </div>


                        <div class="form-group">
                            <label for="paragraph">{{ __('Url For Button') }}</label>
                            <input class="form-control" type="text" name="url" value="" id="url" >
                        </div>

                            <div class="form-group">
                                <label for="btn_name">{{ __('Button Name')}}</label>
                                <input class="form-control" type="text" name="btn_name" value="" id="btn_name" >
                            </div>


                        <div class="form-group">
                            <label for="catImage">{{ __('Image') }}</label>
                            <div class="custom-file">
                                <input type="file" name="img" class="custom-file-input" id="catImage"  >
                                <label class="custom-file-label"
                                       for="catImage">{{ __('Choose') }} {{__('file') }}</label>
                            </div>
                        </div>
                        <div class="form-group" id="imgPreview" style="display: none">
                            <img src="" class="img-fluid" alt="">
                        </div>

                        <button type="submit" class="btn btn-success float-right">{{ __('Save') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editsliderModal" tabindex="-1" role="dialog" aria-labelledby="sliderModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="sliderModalLabel">{{__('Edit')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" id="fromAction" method="post" action="">
                        @csrf
                        @method('put')

                            <div class="form-group">
                                <label for="header">{{ __('Title')}}</label>
                                <input class="form-control" type="text" name="header" value="" id="header" >
                            </div>

                            <div class="form-group">
                                <label for="paragraph">{{ __('Paragraph')}}</label>
                                <input class="form-control" type="text" name="paragraph" value="" id="paragraph">
                            </div>

                        <div class="form-group">
                            <label for="paragraph">{{ __('Url For Button') }}</label>
                            <input class="form-control" type="text" name="url" value="" id="url" >
                        </div>

                            <div class="form-group">
                                <label for="btn_name">{{__('Button Name') }}</label>
                                <input class="form-control" type="text" name="btn_name" value="" id="btn_name" >
                            </div>

                        <div class="form-group">
                            <label for="catImage">{{ __('Image') }}</label>
                            <div class="custom-file">
                                <input type="file" name="img" class="custom-file-input" id="catImage" >
                                <label class="custom-file-label"
                                       for="catImage">{{ __('Choose') }} {{__('file') }}</label>
                            </div>
                        </div>
                        <div class="form-group" id="imgPreview" >
                            <img src="" class="img-fluid" alt="">
                        </div>

                        <button type="submit" class="btn btn-success float-right">{{ __('Save') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
    <script>
        $('input[type="file"]').on('change', function (e) {
            let fileName = e.target.files[0].name,
                reader = new FileReader();

            $(e.target).siblings('label').text(fileName);

            reader.onload = function (event) {
                $('#imgPreview img').attr('src', event.target.result);
            };

            reader.readAsDataURL(e.target.files[0]);
            $('#imgPreview').show();
        });

        function editImage(header,paragraph,img,url,btn_name,href,event) {
            let modal = $('#editsliderModal');
            modal.find('.modal-body input[name="header"]').val(header);
            modal.find('.modal-body input[name="paragraph"]').val(paragraph);
            modal.find('.modal-body input[name="url"]').val(url);
            modal.find('.modal-body input[name="btn_name"]').val(btn_name);
            $('#imgPreview img').attr('src', img);
            // $('#imgPreview').show();

            modal.find('.modal-body form').attr("action", href);

        };

        function removeSlide(name, url, e) {
            e.preventDefault();
            swal({
                title: "{{ __('Are you sure') }}?",
                text: "{{ __('You are deleting') }} ( " + name + " )",
                // icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: '{{ __('Yes, I am sure!') }}',
                cancelButtonText: "{{ __('No, cancel it') }}"
            }).then(
                function (obj) {
                    if (obj) {
                        // alert(obj);
                        window.location = url;
                    }
                }
            );
        }
    </script>
@stop

