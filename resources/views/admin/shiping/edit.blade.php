@extends('admin.layout.master')
@section('title',__('Shipping'))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                    <h3 class="card-title">{{__('Cities')}}</h3>

                <div class="card-body">
                    <div class="row">
{{--                        <div class="col-md-1 mb-2">--}}
{{--                            <label>{{ __('Add') }}<span--}}
{{--                                    class=" d-sm-none">{{ __('New City') }}</span></label>--}}
{{--                            <button type="button" class="btn btn-block btn-outline-primary"--}}
{{--                                    data-toggle="modal" data-target="#cityModal"><i--}}
{{--                                    class="fa fa-plus"></i></button>--}}
{{--                        </div>--}}
                        <div class="col-md-10" style="text-align: center">
                            <label for="default">{{__('Default Price')}}</label>
                            <input class="form-control form-control-sm" width="200px" type="text" name="default"
                                   placeholder="default value of shipping" value="{{setting('general.shipping')}}" aria-controls="productsTable" style="width: 500px; text-align: center;margin: auto;  margin-bottom: auto;
    margin-bottom: 10px;font-weight: bold;font-size: 15px;" disabled>
                        </div>
                    </div>
                    <table id="productsTable" class="table table-striped dt-responsive nowrap" style="width:100%;text-align: center;">
                        <thead>
                        <tr style="text-align: center;" >
                            <th>{{ __('City') }}</th>
                            <th>{{ __('price') }}</th>
                            <th>{{__('Action')}}</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr >
                                <form action="{{route('admin.shipping.update',$city->id)}}" method="post">
                                    @csrf
                                    <td>
                                        <input  style="width: 200px;margin: auto;display: inline-block" class="form-control" type="text" name="name" value="{{ $city->name}}" required  >
                                    </td>
                                    <td><input placeholder="Default" style="width: 200px;margin: auto;display: inline-block" class="form-control" type="text" name="shipPrice" value="{{ $city->price }}"></td>
                                    <td>
                                        <button  type="submit" class="btn btn-primary" >{{__('Update')}}</button>
                                        <a class="btn btn-danger" href="{{route('admin.settings.delete.shipping',$city->id)}}">{{__('Delete')}}</a>
                                    </td>
                                </form>



                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

{{--    <div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="brandModalLabel"--}}
{{--         aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="categoryModalLabel">{{ __('Add') }} {{ __('City') }}</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    <form enctype="multipart/form-data" method="POST" action="{{ route('admin.shipping.store') }}"--}}
{{--                          accept-charset="utf-8">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group">--}}
{{--                            <label for="name">{{__('City') }}</label>--}}
{{--                            <input class="form-control" id="name" name="name" required>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="name">{{__('Price') }}</label>--}}
{{--                            <input class="form-control" id="name" name="shipPrice" placeholder="{{__('Default')}} {{__('Price')}}">--}}
{{--                        </div>--}}

{{--                        <button type="submit" class="btn btn-primary float-right">{{ __('Save') }}</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@stop
