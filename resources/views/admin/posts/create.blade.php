@extends('admin.layout.master')
@section('title','Create')
@section('content')
    <div class="page-inner">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }} </li>
                @endforeach
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{route('admin.posts.store')}}" enctype="multipart/form-data" file="true">
                    @csrf
                    <div class="card">
                    <div class="card-header">
                        <div class="card-title">{{__('Add Blog')}}</div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="subject">{{ __('Name') }}</label>
                                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter  Name" required>
                                    </div>
                                </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                               <div class="row">
                                   <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="desc"> Full Description</label>
                                                <textarea class="form-control summernote" id="summernote" name="desc" required>
                                                </textarea>
                                            </div>
                                   </div>

                               </div>
                                <div class="form-group">
                                    <label for="img">{{__('Upload Image')}}</label>
                                    <input type="file" name="img[]" class="form-control-file" id="img" required multiple>
                                </div>
                                <div class="form-group" id="imgPreview" >
                                    <img src="" class="img-fluid" alt="" width="300px" height="300px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn btn-success float-right">{{__('Add')}}</button>

                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

@stop
@section('script')
    <script>
        $('input[type="file"]').on('change', function (e) {
            let fileName = e.target.files[0].name,
                reader = new FileReader();

            $(e.target).siblings('label').text(fileName);

            reader.onload = function (event) {
                $('#imgPreview img').attr('src', event.target.result);
            };

            reader.readAsDataURL(e.target.files[0]);
            $('#imgPreview').show();
        });

        $(document).ready(function() {
            $('.summernote').summernote({
                tabsize: 2,
                height: 300,
                minHeight: null,
                maxHeight: null,
                focus: true,
                lang: '{{ config("app.locale") }}',
                toolbar: [
                    // [groupName, [list of button]]
                    ['font', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['style', 'ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['insert', [ 'table','hr','video','link']],
                    ['custom', ['picture']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['misc', ['fullscreen', 'undo', 'redo', 'help', 'codeview']]
                ],
            });
        });


    </script>
@stop
