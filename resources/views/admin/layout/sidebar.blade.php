<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner scroll-content scroll-scrolly_visible">
        <div class="sidebar-content">

            <ul class="nav nav-primary">

                <li class="nav-item">
                    <a href="{{url('admin/slider')}}">
                        <i class="far fa-images"></i>
                        <span class="sub-item">{{__('Slider')}}</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{url('admin/images')}}">
                        <i class="fas fa-image"></i>
                        <p>{{__('Brands')}}</p>
                    </a>

                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#base">
                        <i class="fas fa-layer-group"></i>
                        <p>{{__('Blog')}}</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="base">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="{{url('admin/posts/create')}}">
                                    <span class="sub-item">{{__('Create Blog')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/posts/')}}">
                                    <span class="sub-item">{{__('Blog')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/blogSetting')}}">
                                    <span class="sub-item">{{__('Setting')}}</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#product">
                        <i class="fab fa-product-hunt"></i>
                        <p>{{__('Product')}}</p>
                        <span class="badge badge-danger">
                            {{$views = App\Order::where('status', 'new')->count()}}
                        </span>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="product">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="{{url('admin/products/create')}}">
                                    <span class="sub-item">{{__('Create Product')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/products/')}}">
                                    <span class="sub-item">{{__('All Products')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/categoryProduct')}}">
                                    <span class="sub-item">{{__('Categories')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/attributes/all')}}">
                                    <span class="sub-item">{{__('Sizes')}}</span>
                                </a>
                            </li>

{{--                            <li>--}}
{{--                                <a href="{{url('admin/productSetting')}}">--}}
{{--                                    <span class="sub-item">{{__('Setting')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <li>
                                <a href="{{url('admin/orders')}}">
                                    <span class="sub-item">{{__('Orders')}}</span>
                                </a>

                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#instagram">
                        <i class="fab fa-instagram"></i>
                        <p>{{__('instagram Images')}}</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="instagram">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="{{url('admin/instagrams/create')}}">
                                    <span class="sub-item">{{__('Create')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/instagrams/')}}">
                                    <span class="sub-item">{{__('instagrams')}}</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#twitter">
                        <i class="fab fa-twitter"></i>
                        <p>{{__('twitters')}}</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="twitter">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="{{url('admin/twitters/create')}}">
                                    <span class="sub-item">{{__('Create')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/twitters/')}}">
                                    <span class="sub-item">{{__('twitters')}}</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
{{--                <li class="nav-item">--}}
{{--                    <a href="{{url('admin/media')}}">--}}
{{--                        <i class="fas fa-image"></i>--}}
{{--                        <p>{{__('Media')}}</p>--}}
{{--                    </a>--}}

{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{url('admin/themes')}}">--}}
{{--                        <i class="fas fa-tint"></i>--}}
{{--                        <p>{{__('Website Colors')}}</p>--}}
{{--                    </a>--}}

{{--                </li>--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{url('admin/message')}}">--}}
{{--                        <i class="fas fa-book"></i>--}}
{{--                        <p>{{__('Messages')}}</p>--}}
{{--                        <span class="badge badge-danger">--}}
{{--                            {{$views = App\Message::where('views','==', 0)->count()}}--}}
{{--                        </span>--}}
{{--                    </a>--}}

{{--                </li>--}}
                <li class="nav-item">
                   <a href="{{route('admin.shipping.edit')}}">
                       <i class="fas fa-dollar-sign"></i>
                       <p>Shipping</p>
                   </a>
                </li>


            </ul>
        </div>
    </div>
    <!-- model setting-->

</div>
<!-- End Sidebar -->

