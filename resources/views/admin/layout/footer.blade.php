<footer class="footer">
    <div class="container-fluid">

        <div class="copyright m-auto">
            {{__('2020, made with')}} <i class="fa fa-heart heart text-danger"></i> by <a href="#">ecomerce</a>
        </div>
    </div>
</footer>
