@extends('admin.layout.master')
@section('title','brands Images')
@section('content')
    <div class="row row-card-no-pd">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-primary float-right"  data-toggle="modal" data-target="#addbrand">{{__('Add brand')}}</button>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="partners">
                                <ul>
                                    @foreach($partnersLogo as $logo)
                                        <li style="display: inline-block;float: left;width:20%;list-style: none;">
                                            <a href="#">
                                                <img class="img-responsive fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="{{$logo->firstMedia('partnersLogo')->getUrl()}}" style="width:100px;height: 100px ">
                                            </a><br>
                                            <a class="btn btn-danger" href="{{route('admin.deleteLogo',$logo->id)}}">delete</a>
                                            <a href="#" class="btn btn-success " onclick='editImage("{{ $logo->name}}","{{ $logo->desc}}","{{ $logo->firstMedia('partnersLogo')->getUrl()}}","{{ route('admin.updateLogo', $logo->id) }}", event);' data-toggle="modal" data-target="#editLogoModal" >{{ __('Update') }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="addbrand" tabindex="-1" role="dialog" aria-labelledby="addbrand"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('admin.partners.storeLogo')}}" enctype="multipart/form-data" file="true">
                        @csrf

                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">{{__('Add brand logo')}}</div>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="name">{{__('Name')}}</label>
                                            <input type="text" name="name" class="form-control" id="name" >
                                        </div>
                                        <div class="form-group">
                                            <label for="desc">{{__('Description')}}</label>
                                            <input type="text" name="desc" class="form-control" id="desc" >
                                        </div>
                                        <div class="form-group">
                                            <label for="img">{{__('Upload logo Image')}}</label>
                                            <input type="file" name="img" class="form-control-file" id="img" required>
                                        </div>
                                        <div class="form-group" id="imgPreview" >
                                            <img src="" class="img-fluid" alt="" width="300px" height="300px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <button type="submit" class="btn btn-success float-right">{{__('Add')}}</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="editLogoModal" tabindex="-1" role="dialog" aria-labelledby="editLogoModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editLogoModal">{{__('Update')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="fromAction" method="post" action="">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="name">{{__('Name')}}</label>
                        <input type="text" name="name" class="form-control" value="" id="name" >
                    </div>
                    <div class="form-group">
                        <label for="desc">{{__('Description')}}</label>
                        <input type="text" name="desc" class="form-control" value="" id="desc" >
                    </div>


                    <div class="form-group">
                        <label for="logoImage">{{ __('Image') }}</label>
                        <div class="custom-file">
                            <input type="file" name="img" class="custom-file-input" id="logoImage"  >
                            <label class="custom-file-label"
                                   for="logoImage">{{ __('Choose') }} {{__('file') }}</label>
                        </div>
                    </div>
                    <div class="form-group" id="imgPreview" >
                        <img src="" class="img-fluid" alt="">
                    </div>

                    <button type="submit" class="btn btn-success float-right">{{ __('Save') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    <script>
        $('input[type="file"]').on('change', function (e) {
            let fileName = e.target.files[0].name,
                reader = new FileReader();

            $(e.target).siblings('label').text(fileName);

            reader.onload = function (event) {
                $('#imgPreview img').attr('src', event.target.result);
            };

            reader.readAsDataURL(e.target.files[0]);
            $('#imgPreview').show();
        });
        function editImage(name,desc,img,href,event) {
            let modal = $('#editLogoModal');
            modal.find('.modal-body input[name="name"]').val(name);
            modal.find('.modal-body input[name="desc"]').val(desc);
            $('#imgPreview img').attr('src', img);

            modal.find('.modal-body form').attr("action", href);

        };

    </script>
@stop

