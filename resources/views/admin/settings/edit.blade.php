@extends('admin.layout.master')

@section('title', 'Settings | General')
@section('head')

@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#colorWebsite">
                            {{__('Website Color')}}
                        </button>
                        <form method="post" enctype="multipart/form-data"
                              action="{{ route('admin.settings.update') }}">
                            @method('PUT')
                            @csrf
                            @foreach($settings as $key=>$value)
                                @if($key != 'logo')
                                    @if($key != 'tags')
                                        @if( $key != 'top_bar_background_color' && $key != 'top_bar_text_color' && $key != 'top_border_color' && $key != 'navbar_background_color'  && $key != 'footer_background_color' && $key != 'button_color' && $key != 'navbar_text_color' && $key != 'navbar_hover_color' && $key != 'navbar_active_color' && $key !='footer_background_color' && $key != 'footer_bottom_background_color' && $key != 'footer_bottom_text_color' && $key != 'footer_text_color' && $key != 'footer_bottom_border_color' && $key != 'top_bar_visible' && $key != 'phone_number_visible' && $key !=  'email_address_visible')
                                        <span class="mt-5">
                                            <label for="{{ $key }}">{{ucwords(str_replace('_', ' ', $key)) }}</label>
                                            <input type="text" name="{{ $key }}" class="form-control" id="{{ $key }}"
                                                   value="{{ $value }}">
                                        </span>

                                        @endif
                                        @else
                                        <div class="form-group">
                                            <label for="tags">{{ __('SEO keywords') }}</label>
                                            <input type="text" name="tags" class="form-control tags  selectize-{{ $loop->index }}" value="{{$value }}">
                                        </div>

                                    @endif
                                @else
                                    <div class="form-row">
                                        <div class="col-md-2   text-center">
                                            <div class="form-group" id="imgPreview">
                                                <img src="{{ asset('logo') . '/' . $value }}" class="img-fluid"
                                                     alt="{{ $value }}">
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="catImage">{{ __('Logo') }} ( {{ setting('general.logo') }}
                                                                                       )</label>
                                                <div class="custom-file">
                                                    <input type="file" name="logo" class="custom-file-input"
                                                           id="catImage">
                                                    <label class="custom-file-label"
                                                           for="catImage">{{ __('Choose') }} {{__('file') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <button type="submit" class="btn btn-primary float-right">{{ __('Save') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- model setting-->
    <div class="modal fade" id="colorWebsite" tabindex="-1" role="dialog" aria-labelledby="colorWebsite" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="colorWebsite">{{__('Website Color')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data"
                          action="{{ route('admin.settings.update') }}">
                        @method('PUT')
                        @csrf
                        @foreach($settings as $key=>$value)
                            @if( $key != 'top_bar_background_color' && $key != 'top_border_color' && $key != 'top_bar_text_color' && $key != 'navbar_background_color' && $key != 'button_color' && $key != 'navbar_text_color' && $key != 'navbar_hover_color' && $key != 'navbar_active_color' && $key != 'footer_bottom_background_color'  && $key != 'footer_bottom_text_color' && $key !='footer_background_color' && $key != 'footer_text_color' && $key != 'footer_bottom_border_color' )
                                @if($key == 'top_bar_visible' || $key == 'phone_number_visible' || $key ==  'email_address_visible')
                                    <div class="form-group" style="margin: 10px">
                                        <label for="{{ $key }}">{{ucwords(str_replace('_', ' ', $key)) }}</label>
                                        <input type="checkbox" class="status"  name="{{ $key }}"  value="{{$value}}" {{$value == 1 ? 'checked' : 0 }}>
                                        <span style=" " data-href="{{route('admin.visible')}}"  data-checked="{{ __('on') }}" data-unchecked="{{ __('off') }}"></span>

                                    </div>
                                @endif
                            @else
                                <div class="form-group" style="margin: 10px">
                                    <label for="{{ $key }}">{{ucwords(str_replace('_', ' ', $key)) }}</label>
                                    <input type="color" name="{{ $key }}" class="status" value="{{$value }}">
                                </div>
                            @endif

                        @endforeach
                        <button type="submit" class="btn btn-primary float-right">{{ __('Save') }}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end setting -->
@stop

@section('script')
    <script>
        $('input[type="file"]').on('change', function (e) {
            let fileName = e.target.files[0].name,
                reader = new FileReader();

            $(e.target).siblings('label').text(fileName);

            reader.onload = function (event) {
                $('#imgPreview img').attr('src', event.target.result);
            };

            reader.readAsDataURL(e.target.files[0]);
            $('#imgPreview').show();
        });

        $('.tags').selectize({
            persist: false,
            createOnBlur: true,
            create: true
        });
    </script>
@stop
