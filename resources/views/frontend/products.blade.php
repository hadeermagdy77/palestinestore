@extends('frontend.'.setting('theme.site').'.layout.master')
@section('title','products')
@section('content')
    <!-- Start Bradcaump area -->
    @if(count($categoriesProduct) > 0)
    <div class="ht__bradcaump__area bg-image--6" style="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="#">{{$categoriesProduct->name}}</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">{{$categoriesProduct->desc}}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- End Bradcaump area -->

    <section class="htc__product__area shop__page ptb--130 bg__white">
        <div class="container">
            <div class="row">
                  <div class="product__list another-product-style">
                            <!-- Start Single Product -->

                    @foreach($products as $product)
                            <div class="col-md-3 single__pro col-lg-3 cat--1 col-sm-4 col-xs-12">
                                <div class="product foo">
                                    <div class="product__inner">
                                        <div class="pro__thumb">
                                            @if($product->hasMedia('product'))

                                            <a href="{{route('product',$product->id)}}">
                                             <img src="{{ $product->lastMedia('product')->getUrl() }}" alt="product images">
                                            </a>
                                          @endif
                                        </div>
                                      <!--   <div class="product__hover__info">
                                            <ul class="product__action">
                                                <li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-plus"></span></a></li>
                                                <li><a title="Add TO Cart" href="cart.html"><span class="ti-shopping-cart"></span></a></li>
                                                <li><a title="Wishlist" href="wishlist.html"><span class="ti-heart"></span></a></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                    <div class="product__details">
                                        <h2><a href="{{route('product',$product->id)}}">{{$product->name}}</a></h2>
                                        <ul class="product__price">
                                          @if($product->discount)

                                            <li class="old__price">{{$product->price}}</li>
                                            <li class="new__price">{{$product->discount}}</li>
                                        @else
                                            <li class="new__price">{{$product->price}}</li>

                                        @endif

                                        </ul>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                            <!-- End Single Product -->

                 </div>
              </div>


        </div>
    </section>

@stop

