@extends('frontend.layout.master')
@section('title','blog')
@section('content')
    <!-- Start Bradcaump area -->
    @if($categoryText->hasMedia('about'))
    <div class="ht__bradcaump__area bg-image--4" style="background-image: url({{$categoryText->lastMedia('about')->getUrl() }}); background-size:100% 100%;background-repeat: no-repeat; background-attachment:scroll;background-color:rgba(0, 0, 0, 0)" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">{{$categoryText->name}}</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="#">{{$categoryText->description}}</a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!-- End Bradcaump area -->
    <!-- Start Blog Area -->
    @if(count($posts) > 0)

        <section class="wn__portfolio__area gallery__masonry__activation bg--white mt--40 pb--100">
        <div class="container-fluid">

            <div class="row masonry__wrap">
                <!-- Start Single Portfolio -->
                @foreach($posts as $post)
                    <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 col-12 gallery__item cat--1">
                        <div class="portfolio">
                            @if($post->hasMedia('post'))
                                <div class="thumb">
                                    <a href="{{route('blog',$post->id)}}">
                                        <img src="{{ $post->lastMedia('post')->getUrl() }}" alt="blog images" width="295px" height="196px">
                                    </a>

                                    <div class="search">
                                        <a href="{{ $post->lastMedia('post')->getUrl() }}" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-eye"></i></a>
                                    </div>

                                </div>
                            @endif
                            <div class="content">
                                <h6><a href="{{route('blog',$post->id)}}">{{$post->subject}}</a></h6>

                            </div>
                        </div>
                    </div>
                    <!-- End Single Portfolio -->
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Blog Area -->

    @endif

@stop

