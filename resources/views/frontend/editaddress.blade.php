@extends('frontend.layout.master')
@section('title','edit address')
@section('content')
    <!-- Start profile Area -->
    <section class="wn__checkout__area section-padding--lg bg__white">
        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-12 m-auto">
                    <div class=" mt-4" >
                        <form action="{{route('address.update',$address->id)}}" method="Post">
                            @csrf

                            <div class="customer_details">

                                <div class="customar__field">
                                    <div class="margin_between">
                                        <div class="input_box space_between">
                                            <label>Phone <span>*</span></label>
                                            <input type="text" name="phone" value="{{$address->phone}}" placeholder="Phone Here" required>
                                            @if($errors->has('phone'))
                                                <span style="color: red" >
                                            {{ $errors->first('phone') }}
                                        </span>
                                            @endif
                                        </div>
                                        <div class="input_box space_between">
                                            <label>Company name <span></span></label>
                                            <input type="text" name="company" value="{{$address->company}}">
                                        </div>
                                    </div>

                                    <div class="input_box ">
                                        <label>Country<span>*</span></label>
                                        <select name="country" class="select__option" required>
                                            <option value="">Select Country</option>
                                            @foreach($countries as $country)
                                                <option data-price="{{$country->price ?$country->price : setting('general.shipping') }}" {{$address->country == $country->id ? 'selected':''}} value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input_box ">
                                        <label>City <span>*</span></label>
                                        <input type="text" name="city" value="{{$address->city}}" required>
                                    </div>
                                    <div class="input_box">
                                        <label>Address 1<span>*</span></label>
                                        <input type="text" placeholder="Street address" value="{{$address->address1}}" name="address1" >
                                    </div>
                                    <div class="input_box">
                                        <label>Address 2<span>*</span></label>
                                        <input type="text" placeholder="Street address" name="address2" value="{{$address->address2}}">
                                    </div>
                                    <div class="input_box">
                                        <input type="text" placeholder="Apartment, suite, unit etc. (optional)" name="apartment" value="{{$address->apartment}}">
                                    </div>

                                    <div class="input_box">
                                        <label>Postcode / ZIP <span>*</span></label>
                                        <input type="text" name="zip" value="{{$address->zip}}" required>
                                    </div>

                                    <div class="addtocart__actions">
                                        <button class="btn btn-dark" type="submit" >{{__('Update Address')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End profile Area -->
@stop

