
<!-- QUICKVIEW PRODUCT -->

<div class="modal-product">
    <div class="row">
    <!-- Start product images -->
        <div class="col-lg-6 col-12">
          <div class="product-image">
        @if($product->hasMedia('product'))
{{--            <div class="main-image images" >--}}
                <img  src="{{ $product->firstMedia('product')->getUrl() }}" id="mainImage" style="width:514px!important;height:350px">

                @foreach($product->getMedia('product') as $image)
                    <a  onclick="changeImage('{{ $image->getUrl() }}')">
                        <img src="{{ $image->getUrl() }}" style="width:100px;height:100px;padding-bottom: 5px;padding-top: 5px" >

                    </a>

                @endforeach
{{--            </div>--}}
        @endif
    </div>
        </div>
    <!-- end product images -->

        <div class="col-lg-6 col-12">
          <div class="product-info">
           <h1>{{$product->name}}</h1>
        <div class="price-box-3">
            @if($product->discountPrice)
                <div class="s-price-box">
                    <span class="new-price">$ {{$product->discountPrice}}</span>
                    <span class="old-price">$ {{$product->price}}</span>
                    @else
                        <span class="old-price">$ {{$product->price}}</span>
                </div>
            @endif

        </div>
        <div class="quick-desc" style="
                             width: 300px;
                              white-space: nowrap;
                              overflow: hidden;
                              text-overflow: ellipsis;">
            {!! $product->desc !!}
        </div>


                <form method="Post" action="{{route('product.addToCart')}}">
                    @csrf
                    <input type="hidden" name="product" value="{{$product->id}}">
                    @if($product->sizes->count() > 0 )
                        <div class="select__size">
                            <h2>Select size</h2>
                            <select class="form-control" name="size"  required>
                                <option value="" >{{ __('Select Size') }}</option>
                                @foreach($product->sizes as $attribute)
                                    <option value="{{$attribute->id}}">{{ $attribute->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <div class="box-tocart d-flex" style="margin-top:20px">

                    <span>Qty</span>
                    <input id="qty" class="input-text qty" name="qty" min="1" value="1" title="Qty" type="number">
                    </div>
                    <div class="addtocart-btn">
                        <button class="example_e" type="submit" title="Add to Cart">Add to Cart</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@section('scripts')
    <script>
        function changeImage(image) {
            $('#mainImage').attr('src', image);
        }
        </script>
@stop
