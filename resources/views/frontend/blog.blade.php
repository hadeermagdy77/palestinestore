@extends('frontend.layout.master')
@section('title','blog')
@section('content')

    <div class="page-blog-details section-padding--lg bg--white" style="margin-top: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="blog-details content">
                        <article class="blog-post-details">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @if($post->hasMedia('post'))
                                        @foreach($post->getMedia('post') as $image)
                                            @if($loop->first)
                                            <div class="carousel-item active">
                                                <img src="{{ $image->getUrl() }}" class="d-block w-100" height="450px" alt="...">
                                            </div>
                                            @else
                                                <div class="carousel-item">
                                                    <img src="{{ $image->getUrl() }}" class="d-block w-100" height="450px" alt="...">
                                                </div>
                                            @endif
                                            @endforeach
                                    @endif
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                            <div class="post_wrapper" style="margin-top:20px">
                                <div class="post_header">
                                    <h2>{{$post->subject}}</h2>
                                    <div class="blog-date-categori">
                                        <ul>
                                            <li> {{$post->created_at->format('M d Y')}}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <p>{!! $post->desc !!}</p>
                                </div>

                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
