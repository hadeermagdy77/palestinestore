﻿@extends('frontend.layout.master')
@section('title','Cart')
@section('content')
    @if(Cart::count() > 0)
        <!-- cart-main-area start -->
        <div class="cart-main-area section-padding--lg bg--white">
            <div class="container">
                <div class="row">

                        <div class="col-md-12 col-sm-12 ol-lg-12">
{{--                            <form action="" method="get">--}}

                            <div class="table-content wnro__table table-responsive">
                                <table>
                                    <thead>
                                        <tr class="title-top">
                                            <th class="product-thumbnail">Image</th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
{{--                                            <th class="product-subtotal">Total</th>--}}
                                            <th class="product-remove">Remove</th>
                                        </tr>
                                    </thead>
                                    @foreach( Cart::content() as $item)
                                        @php $product =  \App\Product::find($item->id);
                                                        $option = $product->sizes->where('id',$item->options->first())->first();
                                                        $size = null;
                                                         if ($option){
                                                            $size = $option->name;
                                                        }
                                        @endphp
                                    <tbody>

                                        <tr>
                                            <td class="product-thumbnail">
                                                @if($product->hasMedia('product'))
                                                    <a href="{{route('product',$product->slug)}}" >
                                                        <img width="55px" height="63px"  src="{{$product->firstMedia('product')->getUrl()}}" alt="product image">
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="product-name"><a href="{{route('product',$product->slug)}}">{{$item->name}}</a></td>
                                            <td class="product-price"><span class="amount">${{$item->price}}</span></td>
                                          <form action="{{route('cart.update',$item->rowId)}}" method="get" enctype="multipart/form-data">
                                              @csrf
                                              <td class="product-quantity">
                                                  <input type="number" name="qty" value="{{$item->qty}}">
                                                  <button type="submit" class="btn btn-primary">Update</button>
                                              </td>
                                          </form>
                                            <td class="product-remove"><a href="{{route('product.deleteFromCart',$item->rowId)}}">X</a></td>
                                        </tr>

                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
{{--                        </form>--}}
                        <div class="cartbox__btn">
                            <ul class="cart__btn__list d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">

                                <li><a href="{{route('checkout')}}">Check Out</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <div class="cartbox__total__area">
                            <div class="cartbox-total d-flex justify-content-between">
                                <ul class="cart__total__list">
{{--                                    <li>Cart total</li>--}}
                                    <li>Sub Total</li>
                                </ul>
                                <ul class="cart__total__amount">
{{--                                    <li>{{($item->price * $item->qty)}}</li>--}}
                                    <li>{{Cart::Subtotal()}}</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- cart-main-area end -->
    @endif
@stop

@section('scripts')
<script>
    function increment_quantity(cart_id) {
        var inputQuantityElement = $("#input-quantity-"+cart_id);
        var newQuantity = parseInt($(inputQuantityElement).val())+1;
        save_to_db(cart_id, newQuantity);
    }

    function decrement_quantity(cart_id) {
        var inputQuantityElement = $("#input-quantity-"+cart_id);
        if($(inputQuantityElement).val() > 1)
        {
            var newQuantity = parseInt($(inputQuantityElement).val()) - 1;
            save_to_db(cart_id, newQuantity);
        }
    }

    function save_to_db(cart_id, new_quantity) {
        var inputQuantityElement = $("#input-quantity-"+cart_id);
        $.ajax({
            url : "update_cart_quantity.php",
            data : "cart_id="+cart_id+"&new_quantity="+new_quantity,
            type : 'post',
            success : function(response) {
                $(inputQuantityElement).val(new_quantity);
            }
        });
    }
</script>
@stop
