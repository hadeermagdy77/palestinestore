@if(count($twitters) > 0)
    <!-- twiter -->
<section class="twiter" style="margin-bottom: 30px;margin-top: 40px">
    <div class="container">
        <a href="#"><h3 style="margin-bottom: 20px">Tweets</h3></a>
        <div class="row">
            @foreach($twitters as $twitter)
            <div class="col-md-6">
                <div class="twiter-det">
                    <a href="{{$twitter->twitterLink}}"> <h6 style="margin-bottom: 20px">{{$twitter->name}}</h6></a>
                @if($twitter->hasMedia('twitter'))
{{--                @if($twitter->getMedia('twitter')->count() == 1)--}}
{{--                            <img src="{{ $twitter->lastMedia('twitter')->getUrl() }}" width="200px" height="100px">--}}
{{--                        @else--}}
                    @foreach($twitter->getMedia('twitter') as $image)
                            <a href="{{$twitter->twitterLink}}">
                                <img src="{{ $image->getUrl() }}" width="150px" height="100px">
                            </a>
                     @endforeach
                @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- twiter -->
@endif
