
@if(count($instagrams) > 0)

<!-- Best Sale Area -->
<section class="best-seel-area pt--80 pb--60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center pb--50">
                    <h2 class="title__be--2"><span class="color--theme">#SNEAKERPOLITICS </span></h2>

                </div>
            </div>
        </div>
    </div>
    <div class="slider center">
        <!-- Single product start -->
        @foreach($instagrams as $instagram)
            <div class="product product__style--3">
                @if($instagram->hasMedia('instagram'))
                    <div class="product__thumb">
                        <a class="first__img" href="{{$instagram->link}}" target="_blank">
                            <img src="{{ $instagram->lastMedia('instagram')->getUrl() }}" alt="product image" style="height: 123px">
                        </a>
                    </div>
                @endif

            </div>
      @endforeach
        <!-- Single product end -->
    </div>
</section>
<!-- Best Sale Area Area -->
@endif
