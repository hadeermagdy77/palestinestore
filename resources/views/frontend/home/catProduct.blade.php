<section class="cat" style="margin-top:80px;margin-bottom: 20px">
    <div class="container">
        <div class="row">
            @foreach($categoriesProduct->take(3) as $category)
                  
                   <div class="col-md-4">
                    <div class="cat-item">
                        <a class="product-img" href="{{route('products',$category->id)}}">
                            @if($category->hasMedia('catproduct'))
                                <img src="{{ $category->lastMedia('catproduct')->getUrl() }}" class="img-responsive"  style="width:380px;height:260px">
                            @endif
                            <span class="hover">
                                <h3>{{$category->name}}</h3>
                            </span>
                        </a>
                    </div>
                   </div>

            @endforeach
        </div>

    </div>
</section>
