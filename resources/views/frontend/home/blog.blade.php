@if(count($posts) > 0)

    <!-- Start Recent blog Area -->
<section class="wn__recent__post bg--gray ptb--80">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if($categoryText->hasMedia('about'))
                    <div class="section__title text-center">
                        <h2 class="title__be--2"><span class="color--theme">{{$categoryText->name}}</span></h2>
                        <p>{{$categoryText->description}}</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="row mt--50">
            @foreach($posts->take(3) as $post)
               <div class="col-md-6 col-lg-4 col-sm-12">
                <div class="post__itam">
                    <div class="content">
                        @if($post->hasMedia('post'))
                              <img src="{{ $post->lastMedia('post')->getUrl() }}" style="width:275px;height: 183px">
                        @endif
                        <h3><a href="{{route('blog',$post->id)}}">{{$post->subject}}</a></h3>
                        <a href="{{route('blog',$post->id)}}" class="btn btn-dark" style="color:#fff;background-color: #e34a4d;
                                              border-color: #e34a4d;">show full artical</a>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- End Recent blog Area -->
@endif
