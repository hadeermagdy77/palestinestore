@extends('frontend.layout.master')
@section('title','contact us')
@section('content')
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area" style="background: url({{asset('frontend/images/cont.jpg')}}) no-repeat scroll center center / cover ;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Contact Us</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="{{url('/')}}">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Contact Us</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Contact Area -->
    <section class="wn_contact_area bg--white pt--80 pb--80">
        <div class="google__map pb--80">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="googleMap">{!! setting('general.map_iframe_link') !!}</div>
                    </div>
                    <div class="col-lg-4 col-12 md-mt-40 sm-mt-40">
                        <div class="wn__address">
                            <h2 class="contact__title">Get office info.</h2>
                                <p>
                                    @if(setting('general.website_description'))
                                        {{setting('general.website_description')}}
                                    @endif
                                </p>
                            <div class="wn__addres__wreapper">

                                <div class="single__address">
                                    <i class="icon-location-pin icons"></i>
                                    <div class="content">
                                        <span>address:</span>
                                        @if(setting('general.address'))
                                        <p>
                                            {{setting('general.address')}}
                                        </p>
                                        @endif
                                    </div>
                                </div>

                                <div class="single__address">
                                    <i class="icon-phone icons"></i>
                                    <div class="content">
                                        <span>Phone Number:</span>
                                        <p>
                                            @if(setting('general.phone_number'))
                                                <a href="tel:{{setting('general.phone')}}">{{setting('general.phone_number')}}</a>
                                            @endif
                                        </p>
                                    </div>
                                </div>

                                <div class="single__address">
                                    <i class="icon-envelope icons"></i>
                                    <div class="content">
                                        <span>Email address:</span>
                                        <p>
                                            @if(setting('general.contact_email'))
                                                <a href="mailto:{{setting('general.contact_email')}}?Subject=email">{{setting('general.contact_email')}}</a>
                                            @endif
                                        </p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End Contact Area -->


@stop

@section('scripts')
<style>
     iframe{
        width: 100%;
        height: 500px;
    }

</style>
@stop
