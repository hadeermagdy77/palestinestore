@extends('frontend.layout.master')
@section('title','Check Out')
@section('content')
<!-- Start Bradcaump area -->
<!-- End Bradcaump area -->
<!-- Start Checkout Area -->
<section class="wn__checkout__area section-padding--lg bg__white">
    <div class="container">

        <div class="row">
            <div class="col-lg-6 col-12">
                <form action="{{route('order.store')}}" method="Post">
                    @csrf

                    <div class="customer_details">
                        <h3>Billing details</h3>
                        <div class="customar__field">
                            <div class="input_box">
                                <label>Saved Addresses <span>*</span></label>
                                <select name="address_id" id="select_address" class="select__option" required>
                                    <option value="0" >Use New Address</option>
                                    @foreach(Auth::user()->addresses as $address)
                                        <option {{ Auth::user()->active_address_id == $address->id ?'selected' : ''}}  value="{{$address->id}}">{{$address->apartment}} , {{$address->address1}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @php $active =  Auth::user()->activeAddress @endphp
                            <div class="margin_between">
                                <div class="input_box space_between">
                                    <label>First name <span>*</span></label>
                                    <input type="text" name="firstName" value="{{$active ?$active->firstName:''}}" required>
                                </div>
                                <div class="input_box space_between">
                                    <label>last name <span></span></label>
                                    <input type="text" name="lastName" value="{{$active ?$active->lastName:''}}">
                                </div>
                            </div>
                            <div class="margin_between">
                                <div class="input_box space_between">
                                    <label>Phone <span>*</span></label>
                                    <input type="text" name="phone" placeholder="Phone Here" required value="{{$active ?$active->phone:''}}">
                                    @if($errors->has('phone'))
                                        <span style="color: red" >
                                            {{ $errors->first('phone') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="input_box space_between">
                                    <label>Email address <span>*</span></label>
                                    <input type="email" name="email" placeholder="Enter Email Here" value="{{Auth::user()->email}}" readonly required>
                                    @if($errors->has('email'))
                                        <span style="color: red" >
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="input_box">
                                <label>Company name <span></span></label>
                                <input type="text" name="company" value="{{$active ?$active->company:''}}">
                            </div>
                            <div class="margin_between">
                                <div class="input_box space_between">
                                    <label>Country<span>*</span></label>
                                    <select name="country" class=" form-control" required>
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option  data-price="{{$country->price ?$country->price : setting('general.shipping') }}" value="{{$country->id}}" {{ $active && $active->country == $country->id ? 'selected':''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input_box space_between">
                                <label>City <span>*</span></label>
                                <input type="text" name="city" required value="{{$active ?$active->city:''}}">
                            </div>
                            </div>
                            <div class="input_box">
                                <label>Address<span>*</span></label>
                                <input type="text" placeholder="Street address" value="{{$active ?$active->address1:''}}" name="address" required>
                            </div>
                            <div class="input_box">
                                <input type="text" value="{{$active ?$active->apartment:''}}" placeholder="Apartment, suite, unit etc. (optional)" name="apartment">
                            </div>

                            <div class="input_box">
                                <label>Postcode / ZIP <span>*</span></label>
                                <input type="text" name="zip" value="{{$active ?$active->zip:''}}" required>
                            </div>

                            <div class="addtocart__actions">
                                <button class="btn btn-dark" type="submit" >{{__('Complete Order')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-6 col-12 md-mt-40 sm-mt-40">
                <div class="wn__order__box">
                    <h3 class="onder__title">Your order</h3>
                    @if(Cart::count() > 0)
                        <ul class="order__total">
                            <li>Product</li>
                            <li>Total</li>
                        </ul>
                        <ul class="order_product">
                            @foreach( Cart::content() as $item)
                                @php $product =  \App\Product::find($item->id);
                                                        $option = $product->sizes->where('id',$item->options->first())->first();
                                                        $size = null;
                                                         if ($option){
                                                            $size = $option->name;
                                                        }
                                @endphp
                            <li>{{$item->name}} {{$size ? $size : ''}} × {{$item->qty}} <span>{{$item->qty*$item->price}}</span></li>

                            @endforeach
                        </ul>
                        <ul class="shipping__method">
                            <li>Cart Subtotal <span id="subtotal" data-price="{{Cart::Subtotal()}}">$ {{Cart::Subtotal()}}</span></li>
                            <li id="total-shipping">Shipping <span id="price">$ {{ setting('general.shipping') }}</span></li>
                        </ul>
                        <ul class="total__amount">
                            <li id="total">Order Total <span id="total-cost">${{Cart::Subtotal() + setting('general.shipping')}}</span></li>
                        </ul>
                    @else

                        <ul class="order_product">
                            <li class="subtotal"> {{__('NO Item Added')}}</li>
                        </ul>
                    @endif

                </div>
                <div id="accordion" class="checkout_accordion mt--30" role="tablist">
                    <div class="payment">
                        <div class="che__header" role="tab" id="headingFour">
                            <a class="collapsed checkout__title" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <span> <img src="{{asset('frontend/images/paypal.png')}}" width="100px" alt="payment images"> </span>
                            </a>
                        </div>
                        <div id="collapseFour" class="collapse show" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="payment-body">After clicking “Complete order”, you will be redirected to PayPal to complete your purchase securely.</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- End Checkout Area -->
@stop
@section('scripts')
    <script>

        $('select[name="country"]').change( function() {
            let price = $('select[name="country"] option:selected').data('price');
            if (price){
                $('#price').remove();
                $('#total-cost').remove();
                console.log($('#subtotal').data('price'));
                $('#total-shipping').append('<span id="price"> $ '+(parseInt(price)) + '  </span>');
                $('#total').append('<span id="total-cost"> $ '+(parseInt(price)+parseInt($('#subtotal').data('price'))) + '  </span>');
            }

        });
        $('select[name="address_id"]').change( function() {
            let address = $('select[name="address_id"] option:selected').val();
            let href = '{{url('updateActiveAddress/')}}/' + address;

                // console.log(href);
                $.ajax({
                    url: href,
                    method: 'post',
                    data: {'_token': "{{ csrf_token() }}"},
                    success: function (data) {
                        $('select[name="country"]').empty();
                        location.reload();
                    }
                });
                        // }

        });
    </script>
@stop
