@extends('frontend.layout.master')
@section('title','index')
@section('content')

    <!-- Start Slider Section-->
    @if(count($slider)>0)
        <div class="slider-area brown__nav slider--15 slide__activation slide__arrow01 owl-carousel owl-theme">
            <!-- Start Single Slide -->
            @foreach($slider as $slide)
                <div class="slide animation__style10 bg-image--1 fullscreen align__center--left" style="background-image:url({{ $slide->firstMedia('slider')->getUrl()}}); ">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider__content">
                                    <div class="contentbox">
                                        <h2 style="color: #fff;font-size: 20px">{{ $slide->header }}</h2>
                                        <p style="color: #fff">{{ $slide->paragraph }}</p>
                                        @if($slide->url !== null)
                                            <a class="shopbtn"  href="{{$slide->url}}">{{$slide->btn_name}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
        <!-- End Single Slide -->
        </div>
    @endif
    <!-- End Slider area -->

    @foreach($activeSection as $active)
        @include('frontend.home.'.$active->type)

    @endforeach


@stop
