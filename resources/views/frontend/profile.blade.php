@extends('frontend.layout.master')
@section('title','profile')
@section('content')
    <!-- Start profile Area -->
    <section class="wn__checkout__area section-padding--lg bg__white">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-12 ">
                    @auth
                    <p style="float: right">{{Auth::user()->name}} /

                    <a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                        {{__('Logout')}}
                    </a></p>
                    @endauth
                    <h3>ACCOUNT DETAILS</h3>
                    <p class="mt-4">{{Auth::user()->email}}</p>
                    <a href="{{route('address')}}">Add Addresses</a>
{{--                    <a href="{{route('address')}}">View Addresses ({{Auth::user()->addresses->count()}})</a>--}}

                </div>

            </div>
            <!-- cart-main-area start -->
{{--           @if(count($addresses) > 0)--}}
            <div class="cart-main-area section-padding--lg bg--white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ol-lg-12">
                            <form action="#">
                                <div class="table-content wnro__table table-responsive">
                                    <table>
                                        <thead>
                                        <tr class="title-top">
                                            <th class="product-thumbnail">address 1</th>
                                            <th class="product-thumbnail">address 2</th>
                                            <th class="product-thumbnail">city</th>
{{--                                            <th class="product-thumbnail">country</th>--}}
                                            <th class="product-thumbnail">Zip</th>
                                            <th class="product-remove">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($addresses as $address)
                                           <tr>
                                            <td class="product-name"><a href="#">{{$address->address1}}</a></td>
                                            <td class="product-name"><a href="#">{{$address->address2}}</a></td>
                                            <td class="product-name"><a href="#">{{$address->city}}</a></td>
{{--                                            <td class="product-name"><a href="#">{{$address->country->name}}</a></td>--}}
                                            <td class="product-name"><a href="#">{{$address->zip}}</a></td>
                                            <td class="product-remove">
                                                <a href="{{route('address.delete',$address->id)}}">delete</a>
                                                <a href="{{route('address.edit',$address->id)}}">edit</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

          <!---end-->
        </div>
    </section>
    <!-- End profile Area -->
@stop

