@extends('frontend.layout.master')
@section('title','brands')
@section('content')
    <!-- Start Shop Page -->
    <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                    <div class="shop__sidebar" style="margin-top: 50px">

                        <aside class="wedget__categories poroduct--tag">
                            <h3>Sort by</h3>
                            <ul class="text-center" >
                                <li style="width: 100%">
                                    <a class="@if(\Request::query('filter')==null) {{'current'}}@endif" style="width: 100%"  href="{{url('category')}}">New Arrivals</a>

                                </li>
                                <li style="width: 100%">
                                    <a class="@if(\Request::query('filter')=='lowPrice') {{'current'}}@endif"  style="width: 100%" href="{{url('category/'.$category->id.'?filter=lowPrice')}}">price : low to high</a>
                                </li>
                                <li style="width: 100%">
                                    <a class="@if(Request::query('filter') == 'highPrice') {{'current'}}@endif"  style="width: 100%" href="{{url('category/'.$category->id.'?filter=highPrice')}}">price : high to low</a>
                                </li>
                                <li style="width: 100%">
                                    <a class="@if(\Request::query('filter')=='A-Z') {{'current'}}@endif"  style="width: 100%" href="{{url('category/'.$category->id.'?filter=A-Z')}}">Alphabetical (A-Z)</a>
                                </li>
                            </ul>
                        </aside>

                        <aside class="wedget__categories pro--range">
                            <h3 class="wedget__title">Filter by price</h3>
                            <div class="content-shopby">
                                <div class="price_filter s-filter clear">
                                    <form action="{{route('products.filter',$category->id)}}" method="POST">
                                        @csrf
                                        <div id="slider-range" ></div>
                                        <div class="slider__range--output" id="price" min="{{$category->products->min('price')}}" max="{{$category->products->max('price')}}">
                                            <div class="price__output--wrap">
                                                <div class="price--output">
                                                    <span>Price :</span><input name="price" min="{{$category->products->min('price')}}"  type="text" id="amount" readonly>
                                                </div>
                                                <div class="price--filter">
                                                    <button  type="submit">Filter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </aside>
                        <aside class="wedget__categories poroduct--tag">
                            <h3 class="wedget__title">Size</h3>
                            <ul>
                                @foreach($attributes as $attribute)
                                    <li><a href="{{route('size',$attribute->id)}}">{{ $attribute->name }}</a></li>
                                @endforeach
                            </ul>
                        </aside>

                    </div>
                </div>
                <div class="col-lg-9 col-12 order-1 order-lg-2">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="shop__list__wrapper d-flex flex-wrap flex-md-nowrap justify-content-between">
                                <h3 style="margin:auto">{{$category->name}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="tab__container">

                        <div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
                            <div class="row">
                            @foreach($products as $product)
                                <!-- Start Single Product -->
                                    <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">

                                        @if($product->hasMedia('product'))
                                            <div class="product__thumb">
                                                <a class="first__img" href="{{route('product',$product->slug)}}">
                                                    <img src="{{ $product->lastMedia('product')->getUrl() }}" alt="product image" height="185px">
                                                </a>
                                                <a class="second__img animation1" href="{{route('product',$product->slug)}}"><img src="{{ $product->lastMedia('product')->getUrl() }}" alt="product image"></a>
                                                @if($product->discount)
                                                    <div class="hot__box">
                                                        <span class="hot-label">SALE</span>
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                        <div class="product__content content--center">
                                            <h4><a href="{{route('product',$product->slug)}}">{{$product->name}}</a></h4>
                                            <ul class="prize d-flex">
                                                @if($product->discount)

                                                    <li>{{$product->discount}}</li>
                                                    <li class="old_prize">{{$product->price}}</li>
                                                @else
                                                    <li>{{$product->price}}</li>

                                                @endif
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" data-href="{{route('product.quickview',$product->id)}}" href="#productmodal"><i class="bi bi-search"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- End Single Product -->
                                @endforeach
                            </div>
                            <ul class="wn__pagination">
                                {{$products->links()}}
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->



    <!-- End Shop Page -->
    <div id="quickview-wrapper">
        <!-- Modal -->
        <div class="modal fade" id="productmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal__container" role="document">
                <div class="modal-content">
                    <div class="modal-header modal__header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END QUICKVIEW PRODUCT -->

@stop

@section('scripts')
    <script>
        function changeImage(image){
            $('#mainImage').attr('src',image);

        }
        $(document).ready(function() {
            // $('#table').dataTable({ "paginate": false});
            $('#productmodal').on('show.bs.modal', function (event) {
                let button = $(event.relatedTarget),

                    href = button.data('href'),
                    reject = button.data('reject'),
                    name = button.data('name'),
                    modal = $(this);
                $.ajax({
                    url: href,
                    method: 'get',
                    success: function (data) {
                        modal.find('.modal-title').text("details");
                        modal.find('.modal-footer form').attr("action", reject);
                        modal.find('.modal-body').html(data);
                        console.log('njk');
                    }
                });
            });
        });

    </script>
@endsection
