@extends('frontend.layout.master')
@section('title','Products')
@section('content')


  <div class="maincontent bg--white pt--80 pb--55" style="margin-top: 50px">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 col-12">
                  <div class="wn__single__product">
                      <div class="row">
                          <div class="col-lg-6 col-12">
                              <div class="wn__fotorama__wrapper">
                                  @if($product->hasMedia('product'))
                                  <div class="fotorama wn__fotorama__action" data-nav="thumbs" >

                                      <img src="{{ $product->lastMedia('product')->getUrl() }}" style="width:450px !important;" alt="">

                                  @foreach($product->getMedia('product') as $image)
                                              <a href="{{ $image->getUrl() }}" >
                                                  <img src="{{ $image->getUrl() }}" style="width:514px !important;" alt="">
                                              </a>

                                          @endforeach
                                  </div>
                                  @endif
                              </div>
                          </div>
                          <div class="col-lg-6 col-12">
                              <div class="product__info__main">
                                  <h1>{{$product->name}}</h1>
                                  <ul class="prize d-flex">
                                      @if($product->discountPrice)
                                      <li style="font-size:30px;color:#e0383b">$ {{$product->discountPrice}}</li>&nbsp;&nbsp;&nbsp;
                                          <li style="color: #7d7d7d;
                                                  text-decoration: line-through;font-size:20px;">$ {{$product->price}}</li> &nbsp; &nbsp;&nbsp;
                                          @else
                                              <li>$ {{$product->price}}</li>

                                      @endif
                                  </ul>

                                  <div class="product__overview">
                                      <p>{!! $product->desc !!}</p>
                                  </div>
                              <form method="Post" action="{{route('product.addToCart')}}">
                                      @csrf
                                  <input type="hidden" name="product" value="{{$product->id}}">

                                  <div class="row">
                                      @if($product->sizes->count() > 0 )
                                        <div class="col-lg-6 col-12">
                                          <div class="pro__dtl__color">
                                              <h4 class="title__5">{{ __('Sizes') }}</h4>
                                              <select class="form-control" name="size"  required>
                                                  <option value="" >{{ __('Select Size') }}</option>
                                                  @foreach($product->sizes as $attribute)
                                                      <option value="{{$attribute->id}}">{{ $attribute->name }}</option>
                                                  @endforeach
                                              </select>
                                          </div>

                                      </div>
                                      @endif

                                  </div>
                                  <div class="box-tocart d-flex" style="margin-top:20px">
                                      <span>Qty</span>
                                      <input id="qty" class="input-text qty" name="qty" min="1" value="1" title="Qty" type="number">
                                      <div class="addtocart__actions">
                                          <button class="example_e" type="submit" title="Add to Cart">Add to Cart</button>

                                      </div>
                                  </div>
                              </form>

                                  <div class="product_meta">
											<span class="posted_in">Category :<a href="{{route('products',$cat->id)}}">{{$cat->name}}</a></span>
                                  </div>
                                  <div class="product-share">
                                      <ul>
                                          {!! Share::currentPage()->facebook()->twitter()->pinterest(); !!}

                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  @if(count($products) > 0)
                  <div class="wn__related__product pt--80 pb--50" style="padding-top:80px">
                         <div class="section__title text-center">
                          <h2 class="title__be--2">Related Products</h2>
                      </div>
                      <div class="row mt--60">
                          <div class="productcategory__slide--2 arrows_style owl-carousel owl-theme">
                              <!-- Start Single Product -->
                              @foreach($products as $product)
                                <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                  <div class="product__thumb">
                                      @if($product->hasMedia('product'))
                                          <a class="first__img" href="{{route('product',$product->id)}}">
                                              <img src="{{ $product->firstMedia('product')->getUrl() }}" alt="product image" >
                                          </a>
                                      @endif
                                      @if($product->discountPrice)
                                          <div class="hot__box">
                                              <span class="hot-label">Sale</span>
                                          </div>
                                      @endif
                                  </div>
                                  <div class="product__content content--center">
                                      <h4>
                                          <a  href="{{route('product',$product->id)}}">{{$product->name}}</a>
                                      </h4>
                                      @if($product->discountPrice)
                                      <ul class="prize d-flex">

                                              <li>{{$product->discountPrice}}</li>
                                              <li class="old_prize">{{$product->price}}</li> &nbsp; &nbsp;&nbsp;
                                          @else
                                              <li>{{$product->price}}</li>

                                      </ul>
                                      @endif

                                      <div class="action">
                                          <div class="actions_inner">
                                              <ul class="add_to_links">
                                                  <li><a class="cart" href="{{route('product.addToCart')}}"><i class="bi bi-shopping-bag4"></i></a></li>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              @endforeach
                                  <!-- Start Single Product -->
                          </div>
                      </div>
                  </div>
                  @endif
              </div>
          </div>
      </div>
  </div>
  <!-- End main Content -->

@stop
@section('scripts')
    <script>
        function changeImage(image){
            $('#mainImage').attr('src',image);

        }
        $(document).ready(function() {

            $('.select2').change( function() {
                // console.log($('.selected'));
                let value = []
                $('.select2').each(function() {

                    if($(this).find(":selected").val() !== ''){

                        value.push($(this).attr('id')+':'+$(this).find(":selected").val() );
                    }else{
                        value.push($(this).attr('id')+':null' );

                    }

                });
                // console.log(value);
                // let value = $(this).val();
                let id = $(this).attr('id');
                let product = {{$product->id}};
                // alert(value)


                if(value) {
                    let ur =  '{{url('/checkStock/')}}';
                    // let url=  ur+'/'+ product +'/'+value;
                    // alert(url);
                    $.ajax({
                        url: ur+'/'+ product +'/'+value,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {


                            console.log(data);
                            // console.log(value);
                            $.each(data[1], function(key, value) {
                                $('#'+value).empty();
                                $('#'+value).append('<option value=""> Select '+ value +'</option>');

                                $.each(data[0], function(key, value2) {
                                    if (value == value2[0]){
                                        $('#'+value2[0]).append('<option value="'+ value2[1] +'">'+ value2[1] +'</option>');
                                    }
                                });
                            });
                            if(data[1].length == 0){
                                let main = '{{url('/products/price/')}}';
                                let url = main+'/'+ product +'/'+value;

                                $.ajax({
                                    url:url,
                                    type: "get",
                                    dataType: "json",
                                    success: function (data) {
                                        $('#price').children().remove();
                                        $('#price').append('<span >{{setting('general.currency')}} ' +data+'</span>')
                                    }
                                } );
                            }

                        }
                    });

                }else{
                    // $('select[name="subCategory"]').empty();
                }
            });

       });
    </script>

@stop


