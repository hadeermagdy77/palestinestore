@extends('frontend.'.setting('theme.site').'.layout.master')
@section('title','Blog categories ')
@section('content')
    @if($categoryText->hasMedia('about'))
<div class="ht__bradcaump__area" style="background-image: url({{$categoryText->lastMedia('about')->getUrl() }}); background-size:100% 100%;background-repeat: no-repeat; background-attachment:scroll;background-color:rgba(0, 0, 0, 0)" >
            <div class="ht__bradcaump__wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="bradcaump__inner text-center">
                                <h2 class="bradcaump-title">{{$categoryText->name}}</h2>
                                <nav class="bradcaump-inner">
                                  <span class="breadcrumb-item active">{{$categoryText->description}}</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <section class="htc__blog__area bg__white pb--130">
            <div class="container">

                <div class="row">
                    <div class="blog__wrap clearfix mt--60 xmt-30">
                        <!-- Start Single Blog -->
                          @foreach($categoriesBlog as $categoryBlog)
                            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                                <div class="blog foo">
                                    <div class="blog__inner">
                                        <div class="blog__thumb">
                                             @if($categoryBlog->hasMedia('catBlog'))
                                                <a href="{{route('categoryBlog',$categoryBlog->id)}}">
                                                    <img src="{{$categoryBlog->lastMedia('catBlog')->getUrl() }}" alt="blog images" style="width:365px;height: 350px">
                                                </a>
                                             @endif

                                        </div>
                                        <div class="blog__hover__info">
                                            <div class="blog__hover__action">
                                                <p class="blog__des"><a href="{{route('categoryBlog',$categoryBlog->id)}}" style="color: #fff">{{$categoryBlog->name}}</a></p>
                                                <ul class="bl__meta">
                                                    <li style="color: #fff">{{$categoryBlog->desc}}</li>

                                                </ul>
                                                <div class="blog__btn">
                                                    <a class="read__more__btn" href="{{route('categoryBlog',$categoryBlog->id)}}" >{{__('Read More')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                        <!-- End Single Blog -->

                    </div>
                </div>

            </div>
        </section>
    @stop
