@extends('frontend.layout.master')
@section('title','brands')
@section('content')
    <!-- start brands-->
@if(count($brands) > 0)
    <section class="brands" style="margin-bottom: 50px">
        <div class="container">
            <h3 style="margin-top:125px">DESIGNERS</h3>
            <div class="row">
                @foreach($brands as $brand)
                   <div class="col-md-3 col-sm-12">
                       @if($brand->hasMedia('partnersLogo'))
                           <div class="image-brand">
                               <a href="{{route('collections.brand',$brand->id)}}">
                                   <img src="{{$brand->firstMedia('partnersLogo')->getUrl() }}" alt="brand" style="width:150px;height:120px">
                               </a>
                          </div>
                       @endif
                </div>
                @endforeach
            </div>
        </div>

    </section>
@endif
    <!-- end brands -->
@stop
