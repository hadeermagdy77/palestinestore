<!-- Header
header__absolute
-->
<header id="wn__header" class="header__area header__absolute sticky__header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                <div class="logo">
                    <a  href="{{url('/')}}">
                        @if(setting('general.logo'))
                            <img src="{{asset('logo/'.setting('general.logo'))}}" alt="logo"  width="85px">
                        @endif
                    </a>
                </div>
            </div>
            <div class="col-lg-8 d-none d-lg-block">
                <nav class="mainmenu__nav">
                    <ul class="meninmenu d-flex justify-content-start">
                        @php
                            use App\Section;
                            $categoryText = Section::Where('type','blog')->first();
                            $categorySection= Section::Where('type','catProduct')->first();
                            $activeSection= App\Section::Where('active',1)->orderBy('order')->get()->except([6,7]);
                        @endphp

                        <li class="drop with--one--item"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li class="drop"><a href="{{url('newReleases')}}">{{__('New Releases')}}</a></li>
                        <li class="drop"><a href="{{url('brands')}}">{{__('Brands')}}</a>
                            @if(count($brands) > 0)

                                <div class="megamenu mega03">
                                    @foreach($brands as $brand)
{{--                                        @dd($loop->iteration  %7 == 0 || $loop->first)--}}
                                        @if($loop->iteration  %7 == 0 || $loop->first)
                                            <ul class="item item03">
                                                @endif
                                                <li><a href="{{route('collections.brand',$brand->id)}}">{{$brand->name}} </a></li>
                                                @if($loop->iteration  %6 == 0 || $loop->iteration == 6)
                                            </ul>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </li>
                        <li class="drop"><a href="{{url('categories')}}">{{__('Categories')}}</a>
                            @if(count($categoriesProduct) > 0)
                            <div class="megamenu dropdown">
                                    <ul class="item item01">
                                        @foreach($categoriesProduct as $category)
                                          <li><a href="{{route('products',$category->id)}}">{{$category->name}} </a></li>
                                        @endforeach
                                    </ul>
                            </div>
                            @endif

                        </li>
                        <li class="drop"><a href="{{url('sale')}}">{{__('Sale')}}</a></li>
                        <li class="drop"><a href="{{url('blog')}}">{{__('Blog')}}</a></li>
                        <li><a href="{{url('contactUs')}}">{{__('Contact')}}</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                <ul class="header__sidebar__right d-flex justify-content-end align-items-center">
                    <li class="shop_search"><a class="search__active" href="{{route('products.search')}}" style="background:rgba(0, 0, 0, 0) url({{asset('frontend/images/icons/search.png')}}) no-repeat scroll 0 center"></a></li>
                    <li class="shopcart"><a class="cartbox_active" href="#" style="background:rgba(0, 0, 0, 0) url({{asset('frontend/images/icons/cart.png')}}) no-repeat scroll 0 center"><span class="product_qun">{{Cart::count()}}</span></a>
                        <!-- Start Shopping Cart -->
                        <div class="block-minicart minicart__active">
                            <div class="minicart-content-wrapper">
                                <div class="micart__close">
                                    <span>close</span>
                                </div>
                                <div class="items-total d-flex justify-content-between">
                                    <span>{{Cart::count()}} items</span>
                                    <span>Cart Subtotal</span>
                                </div>
                                <div class="total_amount text-right">
                                    <span>${{Cart::Subtotal()}}</span>
                                </div>
                                <div class="mini_action checkout">
                                    <a class="checkout__btn" href="{{route('checkout')}}">Go to Checkout</a>
                                </div>
                                <div class="single__items">
                                    <div class="miniproduct">



                                        @if(Cart::count() > 0)
                                            <div class="shp__cart__wrap">
                                                @foreach( Cart::content() as $item)
                                                    @php $product =  \App\Product::find($item->id);
                                                        $option = $product->sizes->where('id',$item->options->first())->first();
                                                        $size = null;
                                                         if ($option){
                                                            $size = $option->name;
                                                        }
                                                    @endphp
                                                    <div class="item01 d-flex mt--20">
                                                        <div class="thumb">
                                                            @if($product->hasMedia('product'))
                                                                <a href="{{route('product',$product->slug)}}" >
                                                                    <img width="55px" height="63px"  src="{{$product->firstMedia('product')->getUrl()}}" alt="product image">
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <div class="content">
                                                            <h6><a href="{{route('product',$product->slug)}}">{{$item->name}} {{$size ? $size : ''}} </a></h6>
                                                            <span class="prize">${{$item->price}}</span>
                                                            <div class="product_prize d-flex justify-content-between">
                                                                <span class="qun">Qty: {{$item->qty}}</span>
                                                                <ul class="d-flex justify-content-end">
                                                                    <li><a href="{{route('product.deleteFromCart',$item->rowId)}}"><i class="zmdi zmdi-delete"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>


                                        @else

                                            <ul class="shoping__total">
                                                <li class="subtotal"> {{__('NO Item Added')}}</li>
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                                <div class="mini_action cart">
                                    <a class="cart__btn" href="{{route('cart')}}">View and edit cart</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Shopping Cart -->
                    </li >
                    <li class="setting__bar__icon"><a class="setting__active" href="#"  style="background:rgba(0, 0, 0, 0) url({{asset('frontend/images/icons/icon_setting.png')}}) no-repeat scroll 0 center"></a>
                        <div class="searchbar__content setting__block">
                            <div class="content-inner">
                                <div class="switcher-currency">
                                    <div class="switcher-options">
                                        <div class="switcher-currency-trigger">
                                            @auth
                                            <span class="currency-trigger"><a  href="{{url('profile')}}">My Account</a></span>

                                            <span class="currency-trigger">
{{--                                                 @if (session('status'))--}}
{{--                                                    <div class="alert alert-success" role="alert">--}}
{{--                                                        {{ session('status') }}--}}
{{--                                                    </div>--}}
{{--                                                @endif--}}


                                                <a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                                    <i class="fa fa-lock"></i> {{__('Logout')}}
                                                </a>

                                            </span>
                                            @else
                                                <span class="currency-trigger"><a  href="{{route('account')}}">login / register</a></span>

                                            @endauth

                                            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Start Mobile Menu -->
        <div class="row d-none">
            <div class="col-lg-12 d-none">
                <nav class="mobilemenu__nav">
                    <ul class="meninmenu">

                        <li><a href="{{url('/')}}">{{__('Home')}}</a></li>
                        <li><a href="{{url('newReleases')}}">{{__('New Releases')}}</a></li>
                        <li><a href="{{url('brands')}}">{{__('brands')}}</a>
                                    <ul>
                                        @foreach($brands as $brand)
                                        <li><a href="{{route('collections.brand',$brand->id)}}">{{$brand->name}} </a></li>
                                        @endforeach
                                    </ul>

                        </li>
                        <li><a href="{{url('categories')}}">{{__('categories')}}</a>
                                <ul>
                                    @foreach($categoriesProduct as $category)
                                        <li><a href="{{route('products',$category->id)}}">{{$category->name}} </a></li>
                                    @endforeach
                                </ul>

                        </li>
                        <li><a href="{{url('sale')}}">{{__('sale')}}</a></li>
                        <li><a href="{{url('blog')}}">{{__('blog')}}</a></li>
                        <li><a href="{{url('contactUs')}}">{{__('Contact')}}</a></li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- End Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none">
        </div>
        <!-- Mobile Menu -->
    </div>
</header>
<!-- //Header -->
<!-- Start Search Popup -->
<div class="brown--color box-search-content search_active block-bg close__top">
    <form id="search_mini_form" class="minisearch" action="{{route('products.search')}}" method="get">
        <div class="field__search">
            <input type="text" placeholder="Search entire store here..."/>
            <div class="action">
                <a href="#"><i class="zmdi zmdi-search"></i></a>
            </div>
        </div>
    </form>
    <div class="close__wrap">
        <span>close</span>
    </div>
</div>
<!-- End Search Popup -->
