<!-- Footer Area -->
<footer id="wn__footer" class="footer__area bg__cat--8 brown--color">
    <div class="footer-static-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__widget footer__menu">
                        <div class="ft__logo">
                            @if(setting('general.logo'))
                                <a href="#">
                                    <img src="{{asset('logo/'.setting('general.logo'))}}" alt="logo"  width="150px" height="150px">
                                </a>
                            @endif
                            <p class="desc">{{ setting('general.website_description') }}</p>
                        </div>
                        <div class="footer__content">
                            <ul class="social__net social__net--2 d-flex justify-content-center">
                                <li>
                                    @if(setting('general.google-plus_page_link'))
                                        <a href="{{setting('general.google-plus_page_link')}}"><i class="bi bi-google"></i></a>
                                    @endif

                                <li>
                                    @if(setting('general.facebook_page_link'))
                                        <a href="{{setting('general.facebook_page_link')}}"><i class="bi bi-facebook"></i></a>
                                    @endif

                                </li>
                                <li>
                                    @if(setting('general.instagram_page_link'))
                                        <a href="{{setting('general.instagram_page_link')}}"><i class="fa fa-instagram"></i></a>
                                    @endif

                                <li>
                                    @if(setting('general.twitter_page_link'))

                                        <a href="{{setting('general.twitter_page_link')}}"><i class="bi bi-twitter">
                                            </i>
                                        </a>
                                    @endif

                                </li>
                                <li>
                                    @if(setting('general.youTube_page_link'))
                                        <a href="{{setting('general.youTube_page_link')}}"><i class="bi bi-youtube"></i></a>
                                    @endif
                                </li>
                                <li>
                                    @if(setting('general.whatsapp_number'))
                                        <a href="{{setting('general.whatsapp_number')}}"><i class="fa fa-whatsapp"></i></a>
                                    @endif
                                </li>
                            </ul>
                            <ul class="mainmenu d-flex justify-content-center">
                                <li><a href="{{url('contactUs')}}">{{__('location')}}</a></li>
                                <ul class="footer-categories">

                                </ul>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="copyright__wrapper">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6 col-md-6 col-sm-12">--}}
{{--                    <div class="copyright">--}}
{{--                        <div class="copy__right__inner text-left">--}}
{{--                            <p>Copyright <i class="fa fa-copyright"></i> <a href="https://freethemescloud.com/">{{setting('general.website_name')}}</a> All Rights Reserved</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 col-md-6 col-sm-12">--}}
{{--                    <div class="payment text-right">--}}
{{--                        <img src="images/icons/payment.png" alt="" />--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</footer>
<!-- //Footer Area -->

