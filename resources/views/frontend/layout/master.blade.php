<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ setting('general.website_name') . ' | ' }} @yield('title')</title>
    <meta name="description" content="{{ setting('general.website_description') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="{{ setting('general.tags') }}">


    <!-- Favicons -->
    <link rel="shortcut icon" href="{{asset('logo/'.setting('general.logo'))}}">
    <link rel="apple-touch-icon" href="{{asset('logo/'.setting('general.logo'))}}">

    <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/plugins.css')}}">
    <!-- Cusom css -->
    <link rel="stylesheet" href="{{asset('frontend/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">

    <!-- Modernizer js -->
    <script src="{{asset('frontend/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <style>
        .sticky__header.is-sticky{
            background:{{setting('general.navbar_background_color')}} none repeat scroll 0 0;
        }
        .mainmenu__nav .meninmenu li a,.meninmenu li.drop .megamenu .item li a:hover{
            color: {{setting('general.navbar_text_color')}} ;;
        }
        .mainmenu__nav .meninmenu li a:hover{
            color: {{setting('general.navbar_hover_color')}} ;
        }
        .bg__cat--8 .footer-static-top,.bg__cat--8{
            background:{{setting('general.footer_background_color')}} none repeat scroll 0 0;
        }
        .footer__menu .footer__content .mainmenu li a,.social__net li a,.copy__right__inner p,.desc{
            color: {{setting('general.footer_text_color')}};
        }
        .copy__right__inner a{
            color: {{setting('general.footer_text_color')}};

        }
        .footer-static-top{
            border: solid 1px {{setting('general.footer_bottom_border_color')}};
        }
    </style>
    {!!  setting('general.facebook_pixel')!!}
    {!! setting('general.google_analytic') !!}
</head>

<body>
<!--[if lte IE 9]>
   <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<div class="wrapper" id="wrapper">

    @include('frontend.layout.header')
    @if (Session::has('success'))
        <div class="alert alert-success mt-10 text-center" id="alarmmsg" style="margin:auto;text-align: center;position:absolute;top:80px;right:0;z-index: 10">
            {{ Session::get('success')}}
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger mt-10 text-center" id="alarmmsg" style="margin:auto;text-align: center;position:absolute;top:80px;right:0;z-index: 10">
            {{ Session::get('error')}}
        </div>
    @endif
    @if(count($errors) > 0)
        <div class="alert alert-danger" id="alarmmsg" style="margin:auto;text-align: center;position:absolute;top:80px;right:0;z-index: 10">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    @yield('content')



    @include('frontend.layout.footer')

</div>
<!-- JS Files -->
    <script src="{{asset('frontend/js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('frontend/js/popper.min.js')}}"></script>
    <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontend/js/plugins.js')}}"></script>
    <script src="{{asset('frontend/js/active.js')}}"></script>
    @yield('scripts')
</body>
</html>
