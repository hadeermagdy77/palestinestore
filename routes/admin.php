<?php

use App\Admin;
use Illuminate\Support\Facades\Route;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
})->name('home');
Route::prefix('roles')->group(function () {

    Route::group(['middleware' => ['permission:read role']], function () {
        Route::get('all', 'Admin\RolesController@index')->name('role.all');
    });
    Route::group(['middleware' => ['permission:create role']], function () {
        Route::get('create', 'Admin\RolesController@create')->name('role.create');
        Route::post('create', 'Admin\RolesController@store')->name('role.store');
    });
    Route::group(['middleware' => ['permission:update role']], function () {
        Route::get('edit/{role}', 'Admin\RolesController@edit')->name('role.edit');
        Route::post('edit/{role}', 'Admin\RolesController@update')->name('role.update');
    });
    Route::group(['middleware' => ['permission:delete role']], function () {
        Route::get('delete/{role}', 'Admin\RolesController@destroy')->name('role.destroy');
    });
});
Route::group(['prefix'=>'users'],function () {

    Route::group(['middleware' => ['permission:read user']], function () {
        Route::get('all', 'Admin\UsersController@index')->name('users.all');
    });
    Route::group(['middleware' => ['permission:create user']], function () {
        Route::get('create', 'Admin\UsersController@create')->name('users.create');
        Route::post('create', 'Admin\UsersController@store')->name('users.store');
    });
    Route::group(['middleware' => ['permission:update user']], function () {
        Route::get('edit/{user}', 'Admin\UsersController@edit')->name('users.edit');
        Route::post('edit/{user}', 'Admin\UsersController@update')->name('users.update');
    });
    Route::group(['middleware' => ['permission:delete user']], function () {
        Route::get('delete/{user}', 'Admin\UsersController@destroy')->name('users.destroy');
    });
});
Route::group(['prefix'=>'providers'],function () {

    Route::group(['middleware' => ['permission:read provider']], function () {
        Route::get('all', 'Admin\ProvidersController@index')->name('providers.all');
    });
    Route::group(['middleware' => ['permission:create provider']], function () {
        Route::get('create', 'Admin\ProvidersController@create')->name('providers.create');
        Route::post('create', 'Admin\ProvidersController@store')->name('providers.store');
    });
    Route::group(['middleware' => ['permission:read provider']], function () {
        Route::get('/{provider}', 'Admin\ProvidersController@show')->name('providers.show');
    });
    Route::group(['middleware' => ['permission:update provider']], function () {
        Route::get('edit/{provider}', 'Admin\ProvidersController@edit')->name('providers.edit');
        Route::post('edit/{provider}', 'Admin\ProvidersController@update')->name('providers.update');
    });
    Route::group(['middleware' => ['permission:delete provider']], function () {
        Route::get('delete/{provider}', 'Admin\ProvidersController@destroy')->name('providers.destroy');
    });
});
Route::group(['prefix'=>'tasks'],function () {

//    Route::group(['middleware' => ['permission:read tasks']], function () {
        Route::get('all', 'Admin\TaskController@index')->name('tasks.all');
        Route::get('myTasks', 'Admin\TaskController@myTasks')->name('tasks.marketer');
        Route::get('task/{task}', 'Admin\TaskController@show')->name('tasks.show');
        Route::get('task/{task}/{user}', 'Admin\TaskController@showUser')->name('tasks.user.show');
        Route::get('myTask/{task}', 'Admin\TaskController@myTask')->name('tasks.marketer.show');
        Route::get('myTask/{task}/{status}', 'Admin\TaskController@taskStatus')->name('tasks.marketer.status');
//    });
//    Route::group(['middleware' => ['permission:create tasks']], function () {
        Route::get('create', 'Admin\TaskController@create')->name('tasks.create');
        Route::post('users', 'Admin\TaskController@users')->name('tasks.users');
        Route::post('create', 'Admin\TaskController@store')->name('tasks.store');
//    });
//    Route::group(['middleware' => ['permission:update tasks']], function () {
        Route::get('edit/{task}', 'Admin\TaskController@edit')->name('tasks.edit');
        Route::post('edit/{task}', 'Admin\TaskController@update')->name('tasks.update');
        Route::post('task/comment', 'Admin\TaskController@addComment')->name('tasks.comment');
//    });
//    Route::group(['middleware' => ['permission:delete tasks']], function () {
        Route::get('delete/{task}', 'Admin\TaskController@destroy')->name('tasks.destroy');
//    });
});
Route::group(['middleware' => ['permission:sort sections']], function () {
    Route::get('/sectionSort','Admin\SectionsController@index')->name('sectionSort');
    Route::post('/sectionSort','Admin\SectionsController@store')->name('sectionSort.store');
});
Route::group(['middleware' => ['permission:control themes section']], function () {
    Route::get('themes', 'Admin\ThemesController@index')->name('themes');
    Route::get('activeTheme/{active}', 'Admin\ThemesController@active')->name('activeTheme');
    Route::post('visible', 'Admin\ThemesController@visible')->name('visible');

});
//Route::group(['middleware' => ['permission:edit sections setting|control aboutUs section']], function () {
    Route::put('/section/setting', 'Admin\SectionsController@update')->name('section.setting.update');
//});
//Route::group(['middleware' => ['permission:control partners section']], function () {
    Route::get('brandLogo', 'Admin\PartnersController@partners')->name('partners');
    Route::post('brandLogo', 'Admin\PartnersController@storeLogo')->name('partners.storeLogo');
    Route::post('brandName', 'Admin\PartnersController@store')->name('partners.store');
    Route::get('partnersLogo', 'Admin\PartnersController@logo');
    Route::get('images', 'Admin\PartnersController@showImages');
    Route::get('deleteLogo/{image}', 'Admin\PartnersController@delete')->name('deleteLogo');
    Route::get('deleteBackground/{image}', 'Admin\PartnersController@deleteBackground')->name('deleteBackground');
    Route::put('updateLogo/{image}','Admin\PartnersController@update')->name('updateLogo');
//});


//Route::group(['middleware' => ['permission:control blog section']], function () {
    Route::get('/post/{id}', 'Admin\PostsController@destroy')->name('post.destroy');
    Route::resource('posts', 'Admin\PostsController');
    Route::get('deleteImage/{media}','Admin\PostsController@deleteImage')->name('post.deleteImage');
    Route::resource('BlogCategory', 'Admin\BlogCategoryController');
    Route::get('/catBlog/{id}', 'Admin\BlogCategoryController@destroy')->name('catBlog.destroy');
    Route::get('/blogSetting', 'Admin\PostsController@setting');
    Route::get('blog/deleteImage/{media}','Admin\PostsController@deleteImage')->name('blog.deleteImage');
//    Route::get('product/deleteImage/{media}','Admin\ProductsController@deleteImage')->name('products.deleteImage');


//});

/********** product *********/
//Route::group(['middleware' => ['permission:control products section']], function () {
    Route::resource('products', 'Admin\ProductsController');
    Route::get('/product/{id}', 'Admin\ProductsController@destroy')->name('product.destroy');
    Route::get('product/deleteImage/{media}','Admin\ProductsController@deleteImage')->name('products.deleteImage');

//});
//Route::group(['middleware' => ['permission:control productCategory section']], function () {
    Route::resource('categoryProduct', 'Admin\CategoryProductController');
    Route::get('/catProduct/{id}', 'Admin\CategoryProductController@destroy')->name('catProduct.destroy');
    Route::get('productSetting','Admin\CategoryProductController@setting');
    Route::get('categories/subCategory/{category}','Admin\CategoryProductController@subCategory');
    Route::get('categories/attributes/{category}','Admin\CategoryProductController@attributes');
    Route::get('deleteSize/{id}','Admin\AttributesController@destroy')->name('removeSize');

//});
Route::group(['middleware' => ['permission:approve product']], function () {
    Route::post('product/status/{product}', 'Admin\ProductsController@status')->name('product.status');
});
Route::group(['prefix'=>'attributes'],function () {

//    Route::group(['middleware' => ['permission:control attributes']], function () {
        Route::get('all', 'Admin\AttributesController@index')->name('attributes.all');
        Route::post('create', 'Admin\AttributesController@store')->name('attributes.store');
        Route::put('edit/{attribute}', 'Admin\AttributesController@update')->name('attributes.update');
//        Route::get('delete/{user}', 'Admin\UsersController@destroy')->name('users.destroy');
//    });
});
/*********************end products ************/

//Route::group(['middleware' => ['permission:control slider section']], function () {
    Route::resource('slider', 'Admin\SliderController');
    Route::get('deleteSlider/{slider}', 'Admin\SliderController@destroy')->name('slider.delete');
//});

//Route::group(['middleware' => ['permission:update website settings']], function () {
    Route::get('settings', 'Admin\SettingsController@edit')->name('settings.edit');
    Route::put('/settings', 'Admin\SettingsController@update')->name('settings.update');
//});
/* order and request */
Route::get('orders','Admin\OrderController@index')->name('orders');
Route::get('orders/review/{order}','Admin\OrderController@review')->name('orders.review');
Route::get('orders/{order}/{status}', 'Admin\OrderController@update')->name('orders.update');

Route::get('requests','Admin\RequestsController@service')->name('requests');
Route::get('requestsPage','Admin\RequestsController@page')->name('requestsPage');
/*contact us */
Route::get('message','Admin\MessagesController@index')->name('message');
/* instgram */
Route::group(['middleware' => ['permission:control instagram section']], function () {
    Route::resource('instagrams', 'Admin\InstagramController');
    Route::get('/instagram/{id}', 'Admin\InstagramController@destroy')->name('instagram.destroy');

});
/* twitter */
Route::group(['middleware' => ['permission:control twitter section']], function () {
    Route::resource('twitters', 'Admin\TwitterController');
    Route::get('/twitter/{id}', 'Admin\TwitterController@destroy')->name('twitter.destroy');
    Route::get('deleteImage/{media}','Admin\TwitterController@deleteImage')->name('twitter.deleteImage');


});
/* shiping */
Route::prefix('shipping')->group(function (){
    Route::get('/', 'Admin\ShippingController@edit')->name('shipping.edit');
    Route::post('/{country}', 'Admin\ShippingController@update')->name('shipping.update');
//    Route::post('store', 'Admin\ShippingController@store')->name('shipping.store');
    Route::get('delete/{country}', 'Admin\ShippingController@destroy')->name('settings.delete.shipping');

});
//profile
Route::get('profile','Admin\SettingsController@editProfile')->name('profile.edit');
Route::post('updateProfile','Admin\SettingsController@updateProfile')->name('profile.update');
