<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});




/*route front end */
Route::get('/','Frontend\HomeController@index');
Route::get('pages/{slug}','Frontend\HomeController@page')->name('pages');
Route::get('blog','Frontend\HomeController@blogg')->name('blog');
Route::get('blog/{post}','Frontend\HomeController@blog')->name('blog');

/******** cat product **********/
Route::get('categories','Frontend\HomeController@categoriesProduct')->name('categoryProduct');
//Route::post('categories','Frontend\HomeController@categoriesProduct')->name('categoryProduct');
Route::post('categoriesFilter','Frontend\HomeController@categoriesProduct')->name('categories.fil');
Route::get('search/product','Frontend\HomeController@search')->name('products.search');
Route::get('category/{category}','Frontend\HomeController@productsCategory')->name('products');
Route::post('category/{category}','Frontend\HomeController@productsCategory')->name('products.filter');
Route::get('product/{product}','Frontend\HomeController@product')->name('product');
Route::get('checkStock/{product}/{value}','Frontend\OrderController@checkStock')->name('checkStock');
Route::get('size/{size}','Frontend\HomeController@size')->name('size');
Route::post('size/{size}','Frontend\HomeController@size')->name('categories.filter');
Route::get('sortProduct/{request}','Frontend\HomeController@sortProduct')->name('sortProduct');

/**********  end cat product *********/

Route::get('contactUs',function (){
    return view('frontend.contactUs') ;
});
Route::get('/sectionSort','Frontend\HomeController@sortSection')->name('sectionSort');

Route::get('lang/{locale}', 'LocalizationController@index')->name('language.change');
//Route::get('/','Frontend\HomeController@index')->name('home');

/************** brand *********/
Route::get('brands','Frontend\HomeController@brands')->name('brands');
Route::get('collections/{brand}','Frontend\HomeController@brand')->name('collections.brand');
Route::post('collections/{brand}','Frontend\HomeController@brand')->name('collections.filter');
Route::get('sizeBrand/{brand}/{size}','Frontend\HomeController@sizeBrand')->name('sizeBrand');
Route::post('sizeBrand/{brand}','Frontend\HomeController@sizeBrand')->name('sizeBrand.filter');

/*************** sale ***********/
Route::get('sale','Frontend\HomeController@sale')->name('sale');
Route::post('sale','Frontend\HomeController@sale')->name('sale.filter');
/***** newReleases ***/
Route::get('newReleases','Frontend\HomeController@newReleases')->name('newReleases');
Route::post('newReleases','Frontend\HomeController@newReleases')->name('newReleases.filter');

/* product order form */
Route::post('order','Frontend\OrderController@store')->name('order.store');
Route::post('orderNow','Frontend\OrderController@orderNow')->name('order.now');
Route::post('product/addToCart','Frontend\OrderController@addToCart')->name('product.addToCart');
Route::get('product/deleteFromCart/{id}','Frontend\OrderController@deleteFromCart')->name('product.deleteFromCart');
/************** account *********/
Route::get('account','Frontend\HomeController@account')->name('account');
// quickviwe
Route::get('productRev/{product}','Frontend\HomeController@quickview')->name('product.quickview');

//service request
Route::post('request','Frontend\RequestsController@store')->name('request');
//contact us
Route::post('message','Frontend\MessagesController@store')->name('message');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('checkout', 'Frontend\OrderController@checkout')->name('checkout');
    Route::post('payment', 'Frontend\PayPalController@payment')->name('order.payment');
    Route::get('cancel', 'Frontend\PayPalController@cancel')->name('payment.cancel');
    Route::get('payment/success', 'Frontend\PayPalController@success')->name('payment.success');
    Route::post('/updateActiveAddress/{address}','Frontend\OrderController@updateAddress');

});
Route::get('/home', 'Frontend\HomeController@index')->name('home');
/* cart*/
Route::get('/cart', 'Frontend\HomeController@cart')->name('cart');
Route::get('cart/update/{id}', 'Frontend\OrderController@update')->name('cart.update');
/* profile */
Route::get('/profile','Frontend\UserAddressController@profile')->name('profile');
Route::get('/address','Frontend\UserAddressController@index')->name('address');
Route::post('/store','Frontend\UserAddressController@store')->name('address.store');
Route::get('/edit/{id}','Frontend\UserAddressController@edit')->name('address.edit');
Route::post('/update/{id}','Frontend\UserAddressController@update')->name('address.update');
Route::get('/delete/{id}','Frontend\UserAddressController@destroy')->name('address.delete');



